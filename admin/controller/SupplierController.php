<?php 
	require './model/SupplierDB.php';
	require './model/Pagination.php';

	class SupplierController {
		public function getSuppliers() {
			$url = "?action=list-suppliers&";
			$paginationDB = new Pagination($url);
			$supplierDB = new SupplierDB();

			$start = $paginationDB->start();
			$limit = $paginationDB->limit;
			$totalRecord = $supplierDB->totalRecord();
			$totalPages = $paginationDB->totalPages($totalRecord);
			$listPages = $paginationDB->listPages($totalPages);

			$suppliers = $supplierDB->getSuppliers($start, $limit);
			$class_admin = 'supplier';
			$show = true;

			include './view/suppliers/list-suppliers.php';          
		}

		public function newSupplier() {
			if ($_SERVER['REQUEST_METHOD'] === 'POST') {
				$name = $_POST['name'];
				$address = $_POST['address'];
				$phone = $_POST['phone'];
				$description = $_POST['description'];

				if (isset($_FILES['typical_photo'])) {
					$path = '../upload/suppliers/';
					$tmp_name = $_FILES['typical_photo']['tmp_name'];
					$typical_photo = 'typical_photo'.time().rand(1, 1000000);
					
					move_uploaded_file($tmp_name, $path.$typical_photo);
				}

				$supplierDB = new SupplierDB();
				$supplier = new Supplier(0, $name, $address, $phone, $typical_photo,
					$description);

				$supplierDB->addSupplier($supplier);
			}
			$class_admin = 'supplier';
			header('Location: ?action=list-suppliers');
		}

		public function editSupplier() {
			$supplier_id = $_REQUEST['id'];

			$supplierDB = new SupplierDB();
			$supplier = $supplierDB->getDataSupplierById($supplier_id);

			if ($_SERVER['REQUEST_METHOD'] === 'POST') {
				$name = $_POST['name'];
				$address = $_POST['address'];
				$phone = $_POST['phone'];
				$description = $_POST['description'];

				if (isset($_FILES['typical_photo'])) {
					$path = '../upload/suppliers/';
					$tmp_name = $_FILES['typical_photo']['tmp_name'];
					$typical_photo = 'typical_photo'.time().rand(1, 1000000);

					move_uploaded_file($tmp_name, $path.$typical_photo);
				}

				if ($_FILES['typical_photo']['name'] === "") {
					$typical_photo = $supplier->typical_photo;
				}

				$supplier = new Supplier($supplier->id, $name, $address, $phone,
					$typical_photo, $description);

				$supplierDB->updateSupplier($supplier);
			}
			$class_admin = 'supplier';
			include './view/suppliers/edit-supplier.php';
		}

		public function deleteSupplier() {
			$supplier_id = $_REQUEST['id'];

			$supplierDB = new SupplierDB();
			$supplierDB->deleteSupplier($supplier_id);
			$class_admin = 'supplier';
			header('Location: ?action=list-suppliers');
		}
	}
?>