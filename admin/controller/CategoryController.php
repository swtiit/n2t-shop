<?php
    require_once './model/CategoryDB.php';

    $category = new CategoryController();
    if (isset($_POST['update'])) {
        $category -> updateCategory();
    }

	class CategoryController {
		public function getCategories() {
			$url = "?action=showCategoryList&";
			$paginationDB = new Pagination($url);
			$categoryDB = new CategoryDB();

			$start = $paginationDB->start();
			$limit = $paginationDB->limit;
			$totalRecord = $categoryDB->totalRecord();
			$totalPages = $paginationDB->totalPages($totalRecord);
			$listPages = $paginationDB->listPages($totalPages);

			$categories = $categoryDB->getCategories($start, $limit);
			$class_admin = 'category';
			$show = true;

			include './view/category/list-category.php';
		}

        public function insertCategory(){
            $categoryName = $_POST['name'];
            $categoryDescription = $_POST['description'];
            $show_menu = $_POST['show_menu'];
            $categoryRepresentPhoto = "../upload/categoryRepresentPhoto/" . $_FILES['image']['name'];
            $categoryRepresentPhotoTmpName = $_FILES['image']['tmp_name'];
            $categoryModel = new CategoryDB();
            $categoryModel -> insert($categoryName, $categoryDescription, $categoryRepresentPhoto, $categoryRepresentPhotoTmpName, $show_menu);

            header('Location: ?action=showCategoryList');
        }

        public function showFormUpdate(){
            $categoryId = $_REQUEST['id'];
            $categoryModel = new CategoryDB();
            $category = $categoryModel -> showCategoryById($categoryId);
            $class_admin = 'category';		
            require_once './view/category/categoryUpdate.php';
        }

        public function updateCategory(){
            $categoryId = $_REQUEST['id'];
            $categoryName = $_POST['name'];
            $categoryDescription = $_POST['description'];
            $show_menu = $_POST['show_menu'];
            $categoryRepresentPhoto = "../upload/categoryRepresentPhoto/" . $_FILES['image']['name'];
            $categoryRepresentPhotoName = $_FILES['image']['tmp_name'];
            $categoryModel = new CategoryDB();
            $categoryModel ->update($categoryName, $categoryDescription, $categoryRepresentPhoto, $categoryRepresentPhotoName, $categoryId, $show_menu);
        }

        public function deleteCategory($id){
            $categoryModel = new CategoryDB();
            $categoryModel->deleteCategory($id);
            $class_admin = 'category';
            header('Location: ?action=showCategoryList');
        }
	}
?>