<?php
/**
 * Created by PhpStorm.
 * User: thang
 * Date: 7/28/17
 * Time: 3:41 PM
 */
require_once './model/ProductDB.php';

$productController = new ProductController();

if(isset($_POST['editProduct'])) {
    $productController->editProduct();
    header('Location: ?action=listProducts');
}

class ProductController{
    public function showListProduct(){
      $url = "?action=listProducts&";
			$paginationDB = new Pagination($url);
			$productDB = new ProductDB();

			$start = $paginationDB->start();
			$limit = $paginationDB->limit;
			$totalRecord = $productDB->totalRecord();
			$totalPages = $paginationDB->totalPages($totalRecord);
			$listPages = $paginationDB->listPages($totalPages);

			$products = $productDB->showListProduct($start, $limit);
            $categories = $productDB -> getCategory();
            $suppliers =  $productDB -> getSupplier();
			$class_admin = 'product';
			$show = true;

      require_once "./view/product/ListProduct.php";
    }

    public function showInsertProductForm(){
        $productDB = new ProductDB();
        $categories = $productDB -> getCategory();
        $suppliers =  $productDB -> getSupplier();
        $class_admin = 'product';

    }

    public function insertProduct(){
        $hot = $_POST['hot'];
        $code = $_POST['code'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $description = $_POST['description'];
        $category_id = $_POST['category'];
        $supplier_id = $_POST['supplier'];
        $quantity = $_POST['quantity'];
        $productPhotoTmpName = [];
        $productPhotoUrl = [];
        for ($i = 0 ;$i < count($_FILES['image']['tmp_name']); $i++){
            $productPhotoTmpName[] = $_FILES['image']['tmp_name'][$i];
            $productPhotoUrl[] = "../upload/ProductPhoto/".$_FILES['image']['name'][$i];
        }
        $productDB = new ProductDB();
        $productDB -> insertProduct($code, $name, $price, $description, $category_id, $supplier_id, $quantity ,$productPhotoTmpName, $productPhotoUrl, $hot);
        header('Location: ?action=listProducts');
    }

    public function showEditProductForm(){
        $productDB = new ProductDB();
        $categories = $productDB -> getCategory();
        $suppliers =  $productDB -> getSupplier();
        $product = $productDB->showProductOnUpdateForm($_REQUEST['id']);
        $images = $productDB -> showProductImagesOnUpdateForm($_REQUEST['id']);
        $class_admin = 'product';
        require_once './view/product/EditProduct.php';
    }

    public function editProduct(){
        $hot = $_POST['hot'];
        $code = $_POST['code'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $description = $_POST['description'];
        $category_id = $_POST['category'];
        $supplier_id = $_POST['supplier'];
        $quantity = $_POST['quantity'];
        $productPhotoTmpName = [];
        $productPhotoUrl = [];
        $productPhotoName = [];
        for ($i = 0 ;$i < count($_FILES['image']['tmp_name']); $i++){
            $productPhotoTmpName[] = $_FILES['image']['tmp_name'][$i];
            $productPhotoName = $_FILES['image']['name'][$i];
            $productPhotoUrl[] = "../upload/ProductPhoto/".$_FILES['image']['name'][$i];
        }
        $productDB = new ProductDB();
        $productDB -> updateProduct($code, $name, $price, $description, $category_id, $supplier_id, $quantity ,$productPhotoTmpName, $productPhotoUrl, $productPhotoName, $hot);
    }

    public function deleteProduct(){

        $productId = $_REQUEST['id'];
        $productDB = new ProductDB();
        $productDB -> deleteProduct($productId);
        $class_admin = 'product';
        header('Location: ?action=listProducts');
    }
}