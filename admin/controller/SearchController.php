<?php 
	require_once './model/SearchDB.php';

	class SearchController {
		public function getSuppliers() {
			$searchModel = new SearchDB();
			if (isset($_GET['search'])) {
				$search_content = $_GET['search'];
				$suppliers = $searchModel->getSuppliers($search_content);
				if (count($suppliers) > 0) {
					$show = true;
				} else {
					$show = false;
				}

				include './view/suppliers/list-suppliers.php';
			}
		}

		public function getUsers() {
			$searchModel = new SearchDB();
			if (isset($_GET['search'])) {
				$search_content = $_GET['search'];
				$users = $searchModel->getUsers($search_content);
				if (count($users) > 0) {
					$show = true;
				} else {
					$show = false;
				}

				include './view/users/ListUser.php';
			}
		}

		public function getCategories(){
            $searchModel = new SearchDB();
            if (isset($_GET['search'])) {
                $search_content = $_GET['search'];
                $categories = $searchModel->getCategories($search_content);
                if (count($categories) > 0) {
                    $show = true;
                } else {
                    $show = false;
                }

                include './view/category/list-category.php';
            }
        }

        public function getProducts(){
            $searchModel = new SearchDB();
            if (isset($_GET['search'])) {
                $search_content = $_GET['search'];
                $products = $searchModel->getProducts($search_content);
                if (count($products) > 0) {
                    $show = true;
                } else {
                    $show = false;
                }

                include './view/product/ListProduct.php';
            }
        }
	}
?>