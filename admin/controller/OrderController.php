<?php 
	require './model/OrderDB.php';

	class OrderController {
		public function getOrders(){
			$url = "?action=getOrders&";
			$paginationDB = new Pagination($url);
			$orderDB = new OrderDB();

			$start = $paginationDB->start();
			$limit = $paginationDB->limit;
			$totalRecord = $orderDB->totalRecord();
			$totalPages = $paginationDB->totalPages($totalRecord);
			$listPages = $paginationDB->listPages($totalPages);

			$orders = $orderDB->getOrders($start, $limit);
			$class_admin = 'order';

			include './view/orders/index.php'; 
		}

		public function latchOrder() {
			$order_id = $_GET['id'];

			$orderDB = new OrderDB();
			$orderDB->latchOrder($order_id);

			$class_admin = 'order';

			header('Location: ?action=getOrders');
		}

		public function detailOrder() {
			$order_id = $_GET['id'];

			$orderDB = new OrderDB();
			$order_details = $orderDB->detailOrder($order_id);

			$class_admin = 'order';
			include './view/orders/detail_order.php'; 
		}
	}
?>