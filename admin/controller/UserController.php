<?php
require_once './model/UserDB.php';

class UserController {
	public function signin() {
		$class_admin = 'users';
		include './view/users/login.php';
	}

	public function getUser(){
		$url = "?action=listUser&";
		$paginationDB = new Pagination($url);
		$userDB = new UserDB();

		$start = $paginationDB->start();
		$limit = $paginationDB->limit;
		$totalRecord = $userDB->totalRecord();
		$totalPages = $paginationDB->totalPages($totalRecord);
		$listPages = $paginationDB->listPages($totalPages);

		$users = $userDB->getUser($start, $limit);
		$class_admin = 'user';
		$show = true;

		include './view/users/ListUser.php';
	}

	public function addUser(){
		if ($_SERVER['REQUEST_METHOD'] === 'POST'){
			$username = $_POST['username'];
			$password = $_POST['password'];
			$role = $_POST['role'];
			$userDB = new UserDB();
			$user = new User(0, $username, $password, $role);
			$userDB->addUser($user);
			$message_success = 'Thêm người dùng mới thành công!';
		}
		$class_admin = 'users';
		$message = 'success';
		
		require_once './view/users/InsertUser.php';
	}

	public function editUser(){
		$id = $_GET['id'];
		$userDB = new UserDB();
		$user = $userDB->getDataUserById($id);

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$username = $_POST['username'];
			$password = $_POST['password'];
			$role = $_POST['role'];

			$user = new User($user->id, $username, $password, $role);

			$userDB->editUser($user);
		}
		$class_admin = 'users';
		include './view/users/UpdateUser.php';
  }

	public function deactiveUser(){
		$id = $_GET['id'];
		$class = 'user';

		$userDB = new UserDB();
		$userDB->deactiveUser($id);
		
		header('Location: ?action=listUser');
	}

	public function resetPassword() {
		if (isset($_POST['submit'])) {
			session_start();
			$current_user = $_SESSION['current_user'][0];

			$password_old = $_POST['password_old'];
			$password = $_POST['password'];
			$password_confirm = $_POST['password_confirm'];


			if ($password == "" OR $password_old == "" OR $password_confirm == "") {
				$message = "Không được để trống các mật khẩu!";
			} else {
				if ($current_user['password'] != md5($password_old)) {
					$message = 'Mật khẩu cũ không chính xác!';
				} elseif ($password != $password_confirm) {	
					$message = 'Mật khẩu không trùng nhau!';
				} else {
					$user = new User($current_user['id'], $current_user['username'], 
						$password, $current_user['role']);

					$userDB = new UserDB();
					$userDB->resetPassword($user);
					$message_success = 'Đổi mật khẩu thành công!';
				}
			}
		}

		require_once "./view/users/changePassword.php";
	}

	public function login() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST'){
			$username=$_POST['username'];
			$password=$_POST['password'];

			$userDB = new UserDB();
			$checkLogin = $userDB->checkLogin($username, $password);
			if (!$checkLogin) {
				$error_message = "Tài khoản hoặc mật khẩu không đúng!";
				include './view/users/login.php';  		
			} else {
				session_start();
				$_SESSION['username'] = $username;
				$_SESSION['current_user'] = $checkLogin;

				header('Location: /admin');  
			}
		}
	}

	public function logout() {
		session_start();
		unset($_SESSION['username']);
		unset($_SESSION['current_user']);
	
		header('Location: /admin?action=signin');
	}
}
