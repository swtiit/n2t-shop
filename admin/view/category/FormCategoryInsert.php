<div class="modal fade" id="modal-form-category" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-top">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2>Thêm danh mục</h2>
            </div>
            <div class="modal-body container">
        <div class="col-md-12">
            <form action="?action=insert" method="post" enctype="multipart/form-data" id="insertCategory">
                <h4 style="color: red">Các trường bắt buộc <i class="fa fa-exclamation" aria-hidden="true"></i></h4>
                <div class="row">
                    <label class="col-md-2">Tên danh mục: </label>
                    <input class="col-md-6" type="text" name="name" placeholder="Name" size="70" id="categoryName">
                    <p class="col-md-4" style="color: red;" id="categoryNameError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
                </div>
                <div class="row">
                    <label class="col-md-2">Mô tả: </label>
                    <input class="col-md-6" type="text" name="description" placeholder="Description" size="70">
                </div>
                <div class="row">
                    <label class="col-md-2">Hiển thị trên menu: </label>
                    <label class="radio-inline">
								      <input type="radio" name="show_menu" value="1"> Có
								    </label>
								    <label class="radio-inline">
								      <input type="radio" name="show_menu" value="0" checked="checked"> Không
								    </label>
                </div>
                <div class="row">
                    <label class="col-md-2">Ảnh đại diện: </label>
                    <input class="col-md-6" type="file" name="image" id="gallery-photo-add">
                </div>
                <div class="gallery">
                </div>
                <div class="row">
<!--                    <button class="w3-btn w3-cyan w3-padding-small w3-round col-md-2"><a href='?action=showCategoryList' style="text-decoration: none; color: white">Danh sách danh mục</a></button>-->
                    <p class="col-md-1"></p>
                    <button class="w3-btn w3-green w3-padding-small w3-round col-md-1" type="submit" name="insert" >Thêm</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="../public/js/Category/InsertCategory.js"></script>
<?php
require '../public/template/admin/footer.php';
?>

