<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="../public/css/Category/UpdateCategory.css">
</head>
<body>
<div>
    <?php
    require '../public/template/admin/header.php';
    ?>
</div>
<div class="container">
    <form action="" method="post" enctype="multipart/form-data" href="./admin.php">
        <div class="row">
            <label class="col-md-2">Tên danh mục: </label>
            <input class="w3-input w3-border col-md-6" type="text" name="name" placeholder="Name" size="50" value="<?php echo $category['name']?>">
        </div>
        <div class="row">
            <label class="col-md-2">Mô tả: </label>
            <input class="w3-input w3-border col-md-6" type="text" name="description" placeholder="Description" size="50" value="<?php echo $category['description']?>">
        </div>
        <div class="row">
          <label class="col-md-2">Hiển thị trên menu: </label>
        	<label class="radio-inline">
			      <input type="radio" name="show_menu" value="1" 
			      	<?php echo ($category['show_menu'] == 1) ? "checked" : ""; ?>> Có
			    </label>
			    <label class="radio-inline">
			      <input type="radio" name="show_menu" value="0" checked="checked" 
			      	<?php echo ($category['show_menu'] == 0) ? "checked" : ""; ?>> Không
			    </label>
        </div>
        <div class="row">
            <label class="col-md-2">Ảnh đại diện: </label>
            <input class="col-md-6" type="file" name="image" id="gallery-photo-add">
        </div>
        <div class="gallery">
            <img src="<?php echo $category['typical_photo']?>" class="imgUpload" alt="">
        </div>
        <div class="row">
            <button class="w3-btn w3-blue w3-padding-small w3-round col-md-1" class="w3-btn w3-cyan w3-padding-small w3-round"><a href='?action=showCategoryList' style="text-decoration: none; color: white">Danh sách</a></button>
            <p class="col-md-1"></p>
            <button class="w3-btn w3-green w3-padding-small w3-round col-md-1" class="w3-btn w3-green w3-padding-small w3-round" id="editCategory" type="submit" name="update">Sửa</button>
        </div>
    </form>
</div>
<div>
    <?php
    require '../public/template/admin/footer.php';
    ?>
</div>
<script src="../public/js/Category/InsertCategory.js"></script>
<script>
    $("#editCategory").click(function () {
        alert("Sửa thành công!");
    });
</script>
</body>
</html>


