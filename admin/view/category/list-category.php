<?php
	require '../public/template/admin/header.php';
?>

<div class="container-admin">
  <div class="page-header">
    <h2>Danh mục
        <a href="#" class="add-new" data-toggle="modal"
           data-target="#modal-form-category">
            (<span class="glyphicon glyphicon-plus"></span>
            Thêm mới)
        </a>
        <div class="form-search">
            <form method="GET">
                <input type="hidden" name="action" value="search-categories">
                <div class="input-group">
                    <input type="text" name="search" class="form-control"
                           value="<?php echo isset($search_content) ? $search_content : ""; ?>"
                           placeholder="Tìm kiếm..."/>
                    <div class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </h2>
  </div>
	<?php if (isset($message_error)) { ?>
  	<div class="alert alert-danger" role="alert">
		  <button type="button" class="close" data-dismiss="alert"
		  	aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <strong>Lỗi!</strong> <?php echo $message_error; ?>
		</div>	
  <?php } ?>
<?php if ($show) { ?>
	<div class="panel panel-default table-responsive">
		<table class="table table-striped table-hover table-supplier">
		  <thead>
		    <tr class="align-stt">
		      <th class="align-stt">STT</th>
	         <th>Tên danh mục</th>
	         <th>Ảnh</th>
	         <th>Mô tả</th>
	         <th>Hiển thị menu</th>
	         <th>Thao tác</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php $stt = 1; ?>
		    <?php foreach ($categories as $category) { ?>
					<tr class="align-stt">
						<td class="align-stt"><?php echo $stt ?></td>
						<td>
							<?php echo $category->name ?>
						</td>
						<td><img src="<?php echo $category->typical_photo ?>"
							class="img-supplier"></td>
						<td><?php echo $category->description ?></td>
						<td>
							<?php echo ($category->show_menu == 1) ? "Có" : "Không"; ?>
						</td>
						<td class="align-stt">
							<a href="?action=edit&id=<?php echo $category->id; ?>"
								class="btn btn-primary btn-sm">
				    		<span class="glyphicon glyphicon-edit"></span>
				    		Sửa
				    	</a>
	        		<a href="?action=delete-category&id=<?php echo $category->id; ?>"
	        			onclick="return confirm('Bạn có muốn xóa sản phẩm này không?')"
	        			class="btn btn-danger btn-sm">
								<span class="glyphicon glyphicon-remove-circle"></span>
								Xóa
							</a>
						</td>
					</tr>
					<?php $stt = $stt + 1; ?>
	      <?php } ?>
		  </tbody>
		</table>
		<?php if (isset($listPages)) { ?>
			<div class="paginate">
				<ul class="pagination">
					<?php echo $listPages; ?>
				</ul>
			</div>
		<?php } ?>
	</div>
</div>
<?php } else { ?>
    <h3 class="not_found">Không có kết quả quả nào phù hợp...<p class=""></p></h3>
<?php	} ?>
<script>
    $(".deleteCategory").click(function (e) {
        var boolean = confirm("Bạn có chắc muốn xoá không?");
        if(boolean){
            location.reload();
            alert('Xoá thành công');
        } else {
            e.preventDefault();
        }
    })
</script>
<?php
	require '../public/template/admin/footer.php';
  require_once './view/category/FormCategoryInsert.php';
?>
