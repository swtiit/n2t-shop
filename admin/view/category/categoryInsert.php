<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="../public/css/Category/InsertCategory.css">
</head>
<body>
    <div>
        <?php
        require '../public/template/admin/header.php';
        ?>
    </div>
    <div class="container">
        <div class="col-md-12">
            <form action="" method="post" enctype="multipart/form-data" id="insertCategory">
                <h4 style="color: red">Các trường bắt buộc <i class="fa fa-exclamation" aria-hidden="true"></i></h4>
                <div class="row">
                    <label class="col-md-2">Tên danh mục: </label>
                    <input class="w3-input w3-border col-md-6" type="text" name="name" placeholder="Name" size="70" id="categoryName">
                    <p class="col-md-4" style="color: red;" id="categoryNameError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
                </div>
                <div class="row">
                    <label class="col-md-2">Mô tả: </label>
                    <input class="w3-input w3-border col-md-6" type="text" name="description" placeholder="Description" size="70">
                </div>
                <div class="row">
                    <label class="col-md-2">Hiển thị trên menu: </label>
                    <label class="radio-inline">
								      <input type="radio" name="show_menu" value="1"> Có
								    </label>
								    <label class="radio-inline">
								      <input type="radio" name="show_menu" value="0" checked="checked"> Không
								    </label>
                </div>
                <div class="row">
                    <label class="col-md-2">Ảnh đại diện: </label>
                    <input class="col-md-6" type="file" name="image" id="gallery-photo-add">
                </div>
                <div class="gallery">
                </div>
                <div class="row">
                    <button class="w3-btn w3-cyan w3-padding-small w3-round col-md-2"><a href='?action=showCategoryList' style="text-decoration: none; color: white">Danh sách danh mục</a></button>
                    <p class="col-md-1"></p>
                    <button class="w3-btn w3-green w3-padding-small w3-round col-md-1" type="submit" name="insert" >Thêm</button>
                </div>
            </form>
        </div>
    </div>
    <div>
        <?php
        require '../public/template/admin/footer.php';
        ?>
    </div>
    <script src="../public/js/Category/InsertCategory.js"></script>
</body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 7/25/2017
 * Time: 6:15 PM
 */
?>
