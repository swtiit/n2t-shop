<?php 
	require '../public/template/admin/header.php';
?>

<div class="container-admin">
  <div class="page-header">
    <h2>Chi tiết đơn hàng</h2>
  </div>

	<div class="panel panel-default table-responsive">
		<table class="table table-striped table-hover table-supplier">
		  <thead>
		    <tr>
		      <th class="align-stt">STT</th>
	         <th>Đơn hàng</th>
	         <th>Sản phẩm</th>
	         <th>Ảnh</th>
	         <th>Số lượng</th>
	         <th>Đơn gía</th>
	         <th>Thành tiền</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php $stt = 1; ?>
		    <?php foreach ($order_details as $order_detail) { ?>
					<tr>
						<td class="align-stt"><?php echo $stt ?></td>
						<td class="align-stt"><?php echo $order_detail->order_id ?></td>
						<td class="align-stt"><?php echo $order_detail->product_id ?></td>
						<td class="align-stt"><img src="<?php echo $order_detail->image ?>"
							class="img-supplier"></td>
						<td class="align-stt"><?php echo $order_detail->quantities ?></td>
						<td class="align-stt"><?php echo $order_detail->unit_price ?></td>
						<td class="align-stt"><?php echo $order_detail->total_price ?></td>
					</tr>
					<?php $stt = $stt + 1; ?>
	      <?php } ?>
		  </tbody>
		</table>
	</div>		
</div>

<?php
	require '../public/template/admin/footer.php';
	require './view/suppliers/form_supplier.php'
?>
 