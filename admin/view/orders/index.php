<?php 
	require '../public/template/admin/header.php';
?>

<div class="container-admin">
  <div class="page-header">
    <h2>Đơn hàng</h2>
  </div>

	<div class="panel panel-default table-responsive">
		<table class="table table-striped table-hover table-supplier">
		  <thead>
		    <tr>
		      <th class="align-stt">STT</th>
	         <th>Họ và tên</th>
	         <th>Số điện thoại</th>
	         <th>Địa chỉ</th>
	         <th>Email</th>
	         <th>Trạng thái</th>
	         <th>Thao tác</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php $stt = 1; ?>
		    <?php foreach ($orders as $order) { ?>
					<tr>
						<td class="align-stt"><?php echo $stt ?></td>
						<td class="align-stt"><?php echo $order->fullname ?></td>
						<td class="align-stt"><?php echo $order->phone ?></td>
						<td class="align-stt"><?php echo $order->address ?></td>
						<td class="align-stt"><?php echo $order->email ?></td>
						<?php if ($order->status == 0) { ?>
							<td class="align-stt status-order-not-yet">Chưa chốt</td>
						<?php } else { ?>
							<td class="align-stt status-order-done">Đã chốt</td>	
						<?php } ?>	
						<td class="align-stt">
							<a href="?action=latchOrder&id=<?php echo $order->id; ?>"
								onclick="return confirm('Bạn có muốn chốt đơn hàng này không?')"
								class="btn btn-primary btn-sm">
				    		<span class="glyphicon glyphicon-edit"></span>
				    		Chốt
				    	</a>
	        		<a href="?action=detailOrder&id=<?php echo $order->id; ?>"
	        			class="btn btn-info btn-sm">
								<span class="fa fa-info"></span>
								Chi tiết
							</a>
						</td>
					</tr>
					<?php $stt = $stt + 1; ?>
	      <?php } ?>
		  </tbody>
		</table>
		<?php if (isset($listPages)) { ?>
			<div class="paginate">
				<ul class="pagination">
					<?php echo $listPages; ?>
				</ul>
			</div>
		<?php } ?>
	</div>		
</div>

<?php
	require '../public/template/admin/footer.php';
	require './view/suppliers/form_supplier.php'
?>
 