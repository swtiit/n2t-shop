<?php 
	require '../public/template/admin/header.php';
?>

<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="modal-header">
		  <h2>Sửa nhà cung cấp</h2>
		  <a href="?action=list-suppliers"
				class="btn btn-info"><span class="fa fa-arrow-left"></span>
				Danh sách nhà cung cấp
			</a>
		</div>
		<div class="modal-body">
		  <form  method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label for="name">Tên nhà cung cấp:</label>
					<input type="text" class="form-control" name="name"
						value="<?php echo $supplier->name; ?>">
				</div>
				<div class="form-group">
					<label for="address">Địa chỉ:</label>
					<input type="text" class="form-control" name="address"
						value="<?php echo $supplier->address; ?>">
				</div>
				<div class="form-group">
					<label for="phone">Số điện thoại:</label>
					<input type="text" class="form-control" name="phone"
						value="<?php echo $supplier->phone; ?>">
				</div>
				<div class="form-group">
					<label for="typical_photo">Ảnh đại diện:</label><br/>
					<img src="./upload/suppliers/<?php echo $supplier->typical_photo; ?>"
						class="img-edit-supplier"><br/><br/>
					<label for="typical_photo">Thay đổi ảnh đại diện:</label><br/>
					<span class="btn btn-success btn-file">
				  	Chọn ảnh<input type="file" name="typical_photo" id="file" class="inputfile">
					</span>
					<label for="file" class="inputfiletext"><span></span></label>
				</div>
				<div class="form-group">
					<label for="description">Thông tin khác:</label>
					<input type="text" class="form-control" name="description"
						value="<?php echo $supplier->description; ?>">
				</div>
				<div class="form-group">
					<button type="submit" name="submit" class="btn btn-primary">Thay đổi</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
	require '../public/template/admin/footer.php';
?>