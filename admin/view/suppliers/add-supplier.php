<?php 
	require '../public/template/admin/header.php';
?>

<div class="container">
	<div class="row">
		<div class="col-sm-8 offset-sm-2">
			<h2>Thêm nhà cung cấp</h2>
			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label for="name">Tên nhà cung cấp:</label>
					<input type="text" class="form-control" name="name">
				</div>
				<div class="form-group">
					<label for="address">Địa chỉ:</label>
					<input type="text" class="form-control" name="address">
				</div>
				<div class="form-group">
					<label for="phone">Số điện thoại:</label>
					<input type="text" class="form-control" name="phone">
				</div>
				<div class="form-group">
					<label for="typical_photo">Ảnh đại diện:</label><br/>
					<span class="btn btn-success btn-file">
				  	Chọn ảnh<input type="file" name="typical_photo" id="file" class="inputfile">
					</span>
					<label for="file" class="inputfiletext"><span></span></label>
				</div>
				<div class="form-group">
					<label for="description">Thông tin khác:</label>
					<input type="text" class="form-control" name="description">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Thêm mới</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
	require '../public/template/admin/footer.php';
?>