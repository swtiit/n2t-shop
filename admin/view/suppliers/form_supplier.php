<div class="modal fade" id="modal-form-supplier" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content modal-top">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2>Thêm nhà cung cấp</h2>
      </div>
      <div class="modal-body">
        <form action="?action=add-supplier"  method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label for="name">Tên nhà cung cấp:
							<strong class="notnull"> ( * )</strong></label>
						<input type="text" class="form-control" name="name" id="name-supplier">
					</div>
					<div class="form-group">
						<label for="address">Địa chỉ:</label>
						<input type="text" class="form-control" name="address">
					</div>
					<div class="form-group">
						<label for="phone">Số điện thoại:</label>
						<input type="text" class="form-control" name="phone">
					</div>
					<div class="form-group">
						<label for="typical_photo">Ảnh đại diện:</label><br/>
						<span class="btn btn-success btn-file">
					  	Chọn ảnh<input type="file" name="typical_photo" id="file" class="inputfile">
						</span>
						<label for="file" class="inputfiletext"><span></span></label>
					</div>
					<div class="form-group">
						<label for="description">Thông tin khác:</label>
						<input type="text" class="form-control" name="description">
					</div>
					<div class="form-group">
						<button type="submit" name="submit" class="btn btn-primary">Thêm mới</button>
					</div>
				</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>