<?php 
	require '../public/template/admin/header.php';
?>

<div class="container-admin">
  <div class="page-header">
    <h2>Nhà cung cấp
			<a href="#" class="add-new" data-toggle="modal"
	  		data-target="#modal-form-supplier">
	  		(<span class="glyphicon glyphicon-plus"></span>
	  		Thêm mới)
	  	</a>
	  	<div class="form-search">
	  		<form method="GET"> 
	  			<input type="hidden" name="action" value="search-suppliers">
		      <div class="input-group">
			   		<input type="text" name="search" class="form-control" 
							value="<?php echo isset($search_content) ? $search_content : ""; ?>"
			   			placeholder="Tìm kiếm..."/>
			   		<div class="input-group-btn">
			        <button class="btn btn-primary" type="submit">
			        <span class="glyphicon glyphicon-search"></span>
			        </button>
		   			</div>
		   		</div>
				</form>
	  	</div>
    </h2>
  </div>
 	<div class="btn-add-new">
 	</div>
  <?php if (isset($message)) { ?>
  	<div class="alert alert-info" role="alert">
		  <button type="button" class="close" data-dismiss="alert"
		  	aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <strong>Thành công!</strong> Thêm nhà cung cấp thành công!
		</div>	
  <?php } ?>

	<?php if ($show) { ?>
		<div class="panel panel-default table-responsive">
			<table class="table table-striped table-hover table-supplier">
			  <thead>
			    <tr>
			      <th class="align-stt">STT</th>
		         <th>Tên nhà cung cấp</th>
		         <th>Địa chỉ</th>
		         <th>Số điện thoại</th>
		         <th>Ảnh công ty</th>
		         <th>Thông tin khác</th>
		         <th>Thao tác</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php $stt = 1; ?>
			    <?php foreach ($suppliers as $supplier) { ?>
						<tr>
							<td class="align-stt"><?php echo $stt ?></td>
							<td class="align-stt"><?php echo $supplier->name ?></td>
							<td class="align-stt"><?php echo $supplier->address ?></td>
							<td class="align-stt"><?php echo $supplier->phone ?></td>
							<td class="align-stt"><img src="../upload/suppliers/
								<?php echo $supplier->typical_photo ?>" class="img-supplier"></td>
							<td class="align-stt"><?php echo $supplier->description ?></td>
							<td class="align-stt">
								<a href="?action=edit-supplier&id=<?php echo $supplier->id; ?>"
									class="btn btn-primary btn-sm">
					    		<span class="glyphicon glyphicon-edit"></span>
					    		Sửa
					    	</a>
		        		<a href="?action=delete-supplier&id=<?php echo $supplier->id; ?>"
		        			onclick="return confirm('Bạn có muốn xóa nhà cung cấp này không?')"
		        			class="btn btn-danger btn-sm">
									<span class="glyphicon glyphicon-remove-circle"></span>
									Xóa
								</a>
							</td>
						</tr>

						<?php $stt = $stt + 1; ?>
		      <?php } ?>
			  </tbody>
			</table>
			<?php if (isset($listPages)) { ?>
				<div class="paginate">
					<ul class="pagination">
						<?php echo $listPages; ?>
					</ul>
				</div>
			<?php } ?>
		</div>		
	<?php } else { ?>
		<h3 class="not_found">Không có kết quả quả nào phù hợp...<p class=""></p></h3>
	<?php	} ?>
</div>

<?php
	require '../public/template/admin/footer.php';
	require './view/suppliers/form_supplier.php'
?>
 