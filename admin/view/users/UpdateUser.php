<?php 
	require '../public/template/admin/header.php';
?>

<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="modal-header">
		  <h2>Sửa người dùng</h2>
		  <a href="?action=listUser"
				class="btn btn-info"><span class="fa fa-arrow-left"></span>
				Danh sách người dùng
			</a>
		</div>
		<div class="modal-body">
		  <form  method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label for="name">Tên người sử dụng:</label>
					<input type="text" class="form-control" name="username"
						value="<?php echo $user->username ?>">
				</div>
				<div class="form-group">
					<label for="address">Mật khẩu</label>
					<input type="password" class="form-control" name="password">
				</div>
				<div class="form-group">
					<label for="phone">Quyền</label></br>
					<?php $role = $user->role; ?>
					<label class="radio-inline">
						<input type="radio" name="role" value="1"
							<?php echo ($role == 1) ? "checked" : "" ?>>Supper Admin</label>
					<label class="radio-inline">
						<input type="radio" name="role" value="0"
							<?php echo ($role == 0) ? "checked" : "" ?>>Admin</label>
							<label class="radio-inline">
				</div>
				<div class="form-group">
					<button type="submit" name="submit" class="btn btn-primary">Thay đổi</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
	require '../public/template/admin/footer.php';
?>