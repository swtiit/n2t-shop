<?php
	require '../public/template/admin/header.php';

	function getRole($role) {
		if ($role == 1) {
			echo "Supper Admin";
		} else {
			echo "Admin";
		}
	}
?>

<div class="container-admin">
  <div class="page-header">
    <h2>Danh sách người dùng
			<?php if ($_SESSION['current_user'][0]['role'] == 1) { ?>
				<a href="?action=addUser" class="add-new">
		  		(<span class="glyphicon glyphicon-plus"></span>
		  		Thêm mới)
		  	</a>
			<?php } ?>
	  	<div class="form-search">
	  		<form method="GET"> 
	  			<input type="hidden" name="action" value="search-users">
		      <div class="input-group">
			   		<input type="text" name="search" class="form-control" 
							value="<?php echo isset($search_content) ? $search_content : ""; ?>"
			   			placeholder="Tìm kiếm..."/>
			   		<div class="input-group-btn">
			        <button class="btn btn-primary" type="submit">
			        <span class="glyphicon glyphicon-search"></span>
			        </button>
		   			</div>
		   		</div>
				</form>
	  	</div>
    </h2>
  </div>
 	<div class="btn-add-new">
 	</div>
  <?php if (isset($message)) { ?>
  	<div class="alert alert-info" role="alert">
		  <button type="button" class="close" data-dismiss="alert"
		  	aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <strong>Thành công!</strong> Thêm người dùng thành công!
		</div>	
  <?php } ?>

	<?php if ($show) { ?>
		<div class="panel panel-default table-responsive">
			<table class="table table-striped table-hover table-supplier">
			  <thead>
			    <tr>
			      <th class="align-stt">STT</th>
						<th class="align-stt">Tên người dùng</th>
						<th class="align-stt">Quyền</th>
						<?php if ($_SESSION['current_user'][0]['role'] == 1) { ?>
							<th class="align-stt">Thao tác</th>	
						<?php } ?>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php $stt = 1; ?>
			    <?php foreach ($users as $user) { ?>
						<tr>
							<td class="align-stt"><?php echo $stt ?></td>
							<td class="align-stt"><?php echo $user->username ?></td>
							<td class="align-stt"><?php getRole($user->role); ?></td>
								<?php if ($_SESSION['current_user'][0]['role'] == 1) { ?>
									<td class="align-stt">
										<a href="?action=editUser&id=<?php echo $user->id; ?>"
											class="btn btn-primary btn-sm">
							    		<span class="glyphicon glyphicon-edit"></span>
							    		Sửa
							    	</a>
										<a href="?action=deactiveUser&id=<?php echo $user->id; ?>"
				        			onclick="return confirm('Bạn có muốn xóa người dùng này không?')"
				        			class="btn btn-danger btn-sm">
											<span class="glyphicon glyphicon-remove-circle"></span>
											Xóa
										</a>
									</td>
								<?php } ?>
						</tr>

						<?php $stt = $stt + 1; ?>
		      <?php } ?>
			  </tbody>
			</table>
			<?php if (isset($listPages)) { ?>
				<div class="paginate">
					<ul class="pagination">
						<?php echo $listPages; ?>
					</ul>
				</div>
			<?php } ?>
		</div>		
	<?php } else { ?>
		<h3 class="not_found">Không có kết quả quả nào phù hợp...<p class=""></p></h3>
	<?php	} ?>
</div>

<?php
require '../public/template/admin/footer.php';
?>
