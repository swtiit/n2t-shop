<?php 
	require '../public/template/admin/header.php';
?>

<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="modal-header">
		  <h2>Đổi mật khẩu</h2>
		</div>
		<div class="modal-body">
			<?php if (isset($message)) { ?>
		  	<div class="alert alert-danger" role="alert">
				  <strong>Lỗi!</strong> <?php echo $message; ?>
				</div>	
		  <?php } ?>
		  <?php if (isset($message_success)) { ?>
		  	<div class="alert alert-success" role="alert">
				  <?php echo $message_success; ?>
				</div>	
		  <?php } ?>
		  <form  method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label for="address">Mật khẩu cũ</label>
					<input type="password" class="form-control" name="password_old">
				</div>
				<div class="form-group">
					<label for="address">Mật khẩu mới</label>
					<input type="password" class="form-control" name="password">
				</div>
				<div class="form-group">
					<label for="address">Xác nhận mật khẩu</label>
					<input type="password" class="form-control" name="password_confirm">
				</div>
				<div class="form-group">
					<button type="submit" name="submit" class="btn btn-primary">Thay đổi</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
	require '../public/template/admin/footer.php';
?>