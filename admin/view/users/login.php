<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Log in</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../public/bootstraps/css/bootstrap.min.css">
  <link rel="stylesheet" href="../public/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../public/css/ionicons.min.css">
  <link rel="stylesheet" href="../public/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../public/css/blue.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
	<div class="login-box">
	  <div class="login-logo">
	    <b>Admin</b>
	  </div>

	  <div class="login-box-body">
	  	<p class="login-box-msg"></p>
	  	<?php if (isset($error_message)) { ?>
		  	<div class="alert alert-danger" role="alert">
				  <strong>Lỗi!</strong> <?php echo $error_message; ?>
				</div>	
		  <?php } ?>
	    <form action="?action=login" method="post">
	      <div class="form-group has-feedback">
	        <input type="text" name="username"
	        	class="form-control" placeholder="Tên dăng nhập">
	        <span class="glyphicon glyphicon-user form-control-feedback"></span>
	      </div>
	      <div class="form-group has-feedback">
	        <input type="password" name='password'
	        	class="form-control" placeholder="Mật khẩu">
	        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	      </div>
	      <div class="row">
					 <div class="col-sm-4">
	        </div>       
	        <div class="col-sm-4">
	          <button type="submit" class="btn btn-primary btn-block btn-flat">
	          	Đăng nhập
	          </button>
	        </div>
	      </div>
	    </form>
		</div>
	</div>
</body>
</html>
