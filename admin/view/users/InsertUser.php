<?php 
	require '../public/template/admin/header.php';
?>

<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="modal-header">
		  <h2>Thêm người dùng mới</h2>
		  <a href="?action=listUser"
				class="btn btn-info"><span class="fa fa-arrow-left"></span>
				Danh sách người dùng
			</a>
		</div>
		<div class="modal-body">
			<?php if (isset($message_success)) { ?>
		  	<div class="alert alert-success" role="alert">
				  <?php echo $message_success; ?>
				</div>	
		  <?php } ?>
		  <form  method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label for="name">Tên người dùng:</label>
					<input type="text" class="form-control" name="username">
				</div>
				<div class="form-group">
					<label for="address">Mật khẩu:</label>
					<input type="password" class="form-control" name="password">
				</div>
				<div class="form-group">
					<label>Quyền:</label></br>
					<label class="radio-inline">
						<input type="radio" name="role" value="1">Supper Admin</label>
					<label class="radio-inline">
						<input type="radio" name="role" value="0" checked="checked">Admin</label>
				</div>
				<div class="form-group">
					<button type="submit" name="submit" class="btn btn-primary">Thêm mới</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
	require '../public/template/admin/footer.php';
?>