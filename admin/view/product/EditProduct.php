<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div>
    <?php
    require '../public/template/admin/header.php';
    ?>
</div>
<hr>
<div class="container">
    <form action="" method="post" enctype="multipart/form-data" id="formEditProduct">
        <h4 style="color: red">Các trường bắt buộc <i class="fa fa-exclamation" aria-hidden="true"></i></h4>
        <div class="row">
            <label class="col-md-2">Mã sản phẩm: </label>
            <input class="col-md-6" type="text" value="<?php echo $product['code']; ?>" name="code" placeholder="Code" id="productCode">
            <p class="col-md-4" style="color: red;" id="productCodeError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
        </div>
        <div class="row">
            <label class="col-md-2">Tên sản phẩm: </label>
            <input class="col-md-6" type="text" name="name" placeholder="Name" id="productName" value="<?php echo $product['name']; ?>">
            <p class="col-md-4" style="color: red;" id="productNameError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
        </div>
        <div class="row">
            <label class="col-md-2">Mô tả: </label>
            <input class="col-md-6" type="text" name="description" placeholder="Description" value="<?php echo $product['description']; ?>">
        </div>
        <div class="row">
            <label class="col-md-2">Giá: </label>
            <input class="col-md-1" type="text" name="price" placeholder="Price" id="productPrice" value="<?php echo $product['price']; ?>">
            <p class="col-md-4" style="color: red;" id="productPriceError" id="productPriceError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
        </div>
        <div class="row" id="selectedCategory">
            <label class="col-md-2">Danh mục: </label>
            <input type="hidden" value="<?php echo $product['category_id'] ?>" id="categoryId">
            <select class="col-md-2" name="category">
                <?php foreach ($categories as $category): ?>
                    <option value="<?php echo $category['id']?>"><?php echo $category['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="row" id="selectedSupplier">
            <label class="col-md-2">Nhà cung cấp: </label>
            <input type="hidden" id="supplierId" value="<?php echo $product['supplier_id'] ?>">
            <select class="col-md-2" name="supplier" >
                <?php foreach ($suppliers as $supplier): ?>
                    <option value="<?php echo $supplier['id']?>"><?php echo $supplier['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="row">
            <label class="col-md-2">Số lượng: </label>
            <input class="col-md-1" value="<?php echo $product['quantity']; ?>" placeholder="Quantity" type="text" name="quantity" id="productQuantity">
            <p class="col-md-4" style="color: red;" id="productQuantityError" id="productQuantityError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
        </div>
        <div class="row">
            <label for="" class="col-md-2">Hot</label>
            <input type="radio" name="hot" value="1"> Có
            <input type="radio" name="hot" value="0"> Không
            <input type="hidden" name="checkHot" value="<?php echo $product['hot']?>">
        </div>
        <div class="row">
            <label class="col-md-2">Ảnh: </label>
            <input class="col-md-6" id="gallery-photo-add" type="file" name="image[]" multiple>
        </div>
        <div class="gallery">
            <?php foreach ($images as $image): ?>
                <img src="<?php echo $image['url'] ?>" class="imgUpload" alt="">
            <?php endforeach;?>
        </div>
        <div class="row">
            <a href='?action=listProducts' id="listProduct" class="w3-btn w3-blue w3-padding-small w3-round col-md-1">Danh sách</a>
            <p class="col-md-1"></p>
            <button class="w3-btn w3-green w3-padding-small w3-round col-md-1" type="submit" name="editProduct" id="editProduct">Sửa</button>
        </div>
    </form>
</div>
<div>
    <?php
    require '../public/template/admin/footer.php';
    ?>
</div>
<script src="../public/js/Product/EditProduct.js"></script>
<script>

</script>
</body>
</html>
