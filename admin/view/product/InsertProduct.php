<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="../public/css/Product/InsertProduct.css">
</head>
<body>
<div>
    <?php
    require '../public/template/admin/header.php';
    ?>
</div>
<hr> 

<div class="container">
    <div class="col-md-12">
        <form action="" method="post" enctype="multipart/form-data" id="formInsertProduct">
            <h4 style="color: red">Các trường bắt buộc <i class="fa fa-exclamation" aria-hidden="true"></i></h4>
            <div class="row">
                <label class="col-md-2">Mã sản phẩm: </label>
                <input class="w3-input w3-border col-md-6" type="text" name="code" placeholder="Code" id="productCode">
                <p class="col-md-4" style="color: red;" id="productCodeError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
            </div>
            <div class="row">
                <label class="col-md-2">Tên sản phẩm: </label>
                <input class="w3-input w3-border col-md-6" type="text" name="name" placeholder="Name" id="productName">
                <p class="col-md-4" style="color: red;" id="productNameError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
            </div>
            <div class="row">
                <label class="col-md-2">Mô tả: </label>
                <input class="w3-input w3-border col-md-6" type="text" name="description" placeholder="Description">
            </div>
            <div class="row">
                <label class="col-md-2">Giá: </label>
                <input class="w3-input w3-border col-md-1" type="text" name="price" placeholder="Price" id="productPrice">
                <p class="col-md-4" style="color: red;" id="productPriceError" id="productPriceError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
            </div>
            <div class="row">
                <label class="col-md-2">Danh mục: </label>
                <select class="w3-select w3-border col-md-2" name="category" id="">
                    <?php foreach ($categories as $category): ?>
                        <option value="<?php echo $category['id']?>"><?php echo $category['name']?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="row">
                <label class="col-md-2">Nhà cung cấp: </label>
                <select class="w3-select w3-border col-md-2" name="supplier" id="">
                    <?php foreach ($suppliers as $supplier): ?>
                        <option value="<?php echo $supplier['id']?>"><?php echo $supplier['name']?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="row">
                <label class="col-md-2">Số lượng: </label>
                <input class="w3-input w3-border col-md-1" placeholder="Quantity" type="text" name="quantity" id="productQuantity">
                <p class="col-md-4" style="color: red;" id="productQuantityError" id="productQuantityError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
            </div>
            <div class="row">
                <label for="" class="col-md-2">Hot</label>
                <input type="radio" name="hot" value="1"> Có
                <input type="radio" name="hot" value="0"> Không
                <input type="hidden" name="checkHot" value="<?php echo $product['hot']?>">
            </div>
            <div class="row">
                <label class="col-md-2">Ảnh: </label>
                <input class="col-md-6" type="file" id="gallery-photo-add" name="image[]" multiple>
            </div>
            <div class="gallery">
            </div>
            <div class="row">
                <a href='?action=listProducts' class="w3-btn w3-blue w3-padding-small w3-round col-md-1" id="listProduct">Danh sách</a>
                <p class="col-md-1"></p>
                <button class="w3-btn w3-green w3-padding-small w3-round col-md-1" type="submit" name="insertProduct" id="insertProduct">Thêm</button>
            </div>
        </form>
    </div>
</div>
<div>
    <?php
    require '../public/template/admin/footer.php';
    ?>
</div>
<script src="../public/js/Product/InsertProduct.js"></script>
</body>
</html>
