<div class="modal fade" id="modal-form-product" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-top">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2>Thêm sản phẩm</h2>
            </div>
            <div class="modal-body container">
                <div class="row">
                    <form action="?action=insertProduct" method="post" enctype="multipart/form-data" id="formInsertProduct">
                        <h4 style="color: red">Các trường bắt buộc <i class="fa fa-exclamation" aria-hidden="true"></i></h4>
                        <div class="row">
                            <label class="col-md-2">Mã sản phẩm: </label>
                            <input class="col-md-6" type="text" name="code" placeholder="Code" id="productCode">
                            <p class="col-md-4" style="color: red;" id="productCodeError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
                        </div>
                        <div class="row">
                            <label class="col-md-2">Tên sản phẩm: </label>
                            <input class="col-md-6" type="text" name="name" placeholder="Name" id="productName">
                            <p class="col-md-4" style="color: red;" id="productNameError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
                        </div>
                        <div class="row">
                            <label class="col-md-2">Mô tả: </label>
                            <input class="col-md-6" type="text" name="description" placeholder="Description">
                        </div>
                        <div class="row">
                            <label class="col-md-2">Giá: </label>
                            <input class="col-md-1" type="text" name="price" placeholder="Price" id="productPrice">
                            <p class="col-md-4" style="color: red;" id="productPriceError" id="productPriceError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
                        </div>
                        <div class="row">
                            <label class="col-md-2">Danh mục: </label>
                            <select class="col-md-2" name="category" id="">
                                <?php foreach ($categories as $category): ?>
                                    <option value="<?php echo $category['id']?>"><?php echo $category['name']?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="row">
                            <label class="col-md-2">Nhà cung cấp: </label>
                            <select class="col-md-2" name="supplier" id="">
                                <?php foreach ($suppliers as $supplier): ?>
                                    <option value="<?php echo $supplier['id']?>"><?php echo $supplier['name']?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="row">
                            <label class="col-md-2">Số lượng: </label>
                            <input class="col-md-1" placeholder="Quantity" type="text" name="quantity" id="productQuantity">
                            <p class="col-md-4" style="color: red;" id="productQuantityError" id="productQuantityError"><i class="fa fa-exclamation" aria-hidden="true"></i></p>
                        </div>
                        <div class="row">
                            <label class="col-md-2">Ảnh: </label>
                            <input class="col-md-6" type="file" id="gallery-photo-add" name="image[]" multiple>
                        </div>
                        <div class="gallery">
                        </div>
                        <div class="row">
<!--                            <a href='?action=listProducts' class="w3-btn w3-blue w3-padding-small w3-round col-md-1" id="listProduct">Danh sách</a>-->
                            <p class="col-md-1"></p>
                            <button class="w3-btn w3-green w3-padding-small w3-round col-md-1" type="submit" name="insertProduct" id="insertProduct">Thêm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    <?php
    require '../public/template/admin/footer.php';
    ?>
</div>
<script src="../public/js/Product/InsertProduct.js"></script>
</body>
</html>
