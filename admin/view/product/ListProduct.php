<?php
require '../public/template/admin/header.php';
?>

<div class="container-admin">
	<div class="page-header">
		<h2>Sản phẩm
<!--			<a href="?action=insertProduct" class="btn btn-lg btn-success pull-right">-->
<!--				<span class="glyphicon glyphicon-plus"></span>-->
<!--				Thêm mới-->
<!--			</a>-->
            <a href="#" class="add-new" data-toggle="modal"
               data-target="#modal-form-product">
                (<span class="glyphicon glyphicon-plus"></span>
                Thêm mới)
            </a>
            <div class="form-search">
                <form method="GET">
                    <input type="hidden" name="action" value="search-products">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control"
                               value="<?php echo isset($search_content) ? $search_content : ""; ?>"
                               placeholder="Tìm kiếm..."/>
                        <div class="input-group-btn">
                            <button class="btn btn-primary" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
		</h2>
	</div>
<?php if ($show) { ?>
	<div class="panel panel-default table-responsive">
		<table class="table table-striped table-hover table-supplier">
		  <thead>
		    <tr class="align-stt">
		      <th class="align-stt">STT</th>
	         <th>Mã sản phẩm</th>
	         <th>Tên sản phẩm</th>
	         <th>Giá</th>
	         <th>Danh mục</th>
	         <th>Nhà cung cấp</th>
	         <th>Thao tác</th>
		    </tr>
		  </thead>
		  <tbody>
				<?php $stt = 1; ?>
				<?php foreach ($products as $product) { ?>
					<tr class="align-stt">
						<td class="align-stt"><?php echo $stt ?></td>
						<td>
							<?php echo $product->getCode() ?>
						</td>
						<td><?php echo $product->getName() ?></td>
						<td><?php echo $product->getPrice() ?></td>
						<td><?php echo $product->getCategoryName() ?></td>
						<td><?php echo $product->getSupplierName() ?></td>
						<td class="align-stt">
							<a href="?action=editProduct&id=<?php echo $product->getId() ?>">
								<button type="button" name = "update" class="btn btn-primary btn-sm">
									<span class="glyphicon glyphicon-edit"></span>
									Sửa
								</button>
							</a>
							<a href="?action=deleteProduct&id=<?php echo $product->getId() ?>" class="deleteProduct">
								<button type="button" class="btn btn-danger btn-sm">
									<span class="glyphicon glyphicon-remove-circle"></span>
									Xóa
								</button>
							</a>
						</td>
					</tr>
				<?php $stt = $stt + 1; ?>
				<?php } ?>
			</tbody>
		</table>
		<?php if (isset($listPages)) { ?>
			<div class="paginate">
				<ul class="pagination">
					<?php echo $listPages; ?>
				</ul>
			</div>
		<?php } ?>
	</div>

</div>
<script>
    $(".deleteProduct").click(function (e) {
        var boolean = confirm("Bạn có chắc muốn xoá không?");
        if(boolean){
            location.reload();
            alert('Xoá thành công');
        } else {
            e.preventDefault();
        }
    })
</script>
<?php } else { ?>
    <h3 class="not_found">Không có kết quả quả nào phù hợp...<p class=""></p></h3>
<?php	} ?>
<?php
require '../public/template/admin/footer.php';
require_once './view/product/FormProduct.php';
?>