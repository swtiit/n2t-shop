<?php
/**
 * Created by PhpStorm.
 * User: thang
 * Date: 7/28/17
 * Time: 3:42 PM
 */
require_once 'Database.php';
require_once 'Product.php';


class ProductDB extends Database {
    function showListProduct($start, $limit){
        $db = $this->loadDatabase();

        $query = "SELECT products.id as id ,products.code as code, products.name as name, products.price as price, products.description as description
                  , categories.name as category_name , suppliers.name as supplier_name  FROM products
                  JOIN categories ON products.category_id = categories.id 
                  JOIN suppliers ON products.supplier_id = suppliers.id WHERE products.status = 1 ORDER BY products.id LIMIT {$start},{$limit}";
        $rows = $db -> query($query);
        $rows = $rows -> fetchAll();
        $products = [];
        foreach ($rows as $row){
            $id = $row['id'];
            $code = $row['code'];
            $name = $row['name'];
            $price = $row['price'];
            $category_name = $row['category_name'];
            $supplier_name = $row['supplier_name'];
            $description = $row['description'];

            $product = new Product($code, $name, $price, $category_name, $supplier_name, $description);
            $product -> setId($id);

            $products[] = $product;
        }
        return $products;
    }

    public function totalRecord() {
			$db = $this->loadDatabase();

			$query = "SELECT * FROM products WHERE status = 1";
			$rows = $db->query($query);
			$count = $rows->fetchAll();

			return count($count);
		}

    function getCategory(){
        $db = $this -> loadDatabase();
            $query = "SELECT * FROM categories";
            $categories = $db->query($query);
        return $categories;
    }

    function getSupplier(){
        $db = $this -> loadDatabase();

        $query = "SELECT * FROM suppliers";
        $suppliers = $db->query($query);

        return $suppliers;
    }

    function insertProduct($code, $name, $price, $description, $category_id,
    	$supplier_id, $quantity , $productPhotoTmpName, $productPhotoUrl, $hot){
        $db = $this->loadDatabase();

        $insertProduct = "INSERT INTO products(code, name, price, category_id,
        	supplier_id, description, quantity, hot) VALUES('".$code."', '".$name."',
        	".$price.", ".$category_id.", ".$supplier_id.", '".$description."', ".$quantity.", ".$hot.")";
        $db->exec($insertProduct);

        $query = "SELECT * FROM products WHERE code ='".$code."'";
        $selectProductId = $db->query($query);
        $selectProductId = $selectProductId ->fetch();
        if(isset($_FILES['image'])){
            foreach ($productPhotoUrl as $url){
                $query = "INSERT INTO product_photos(url, product_id) VALUES('".$url."', ".$selectProductId['id'].")";
                $db -> exec($query);
            }
            for ($i = 0; $i < count($productPhotoUrl); $i++){
                move_uploaded_file($productPhotoTmpName[$i],$productPhotoUrl[$i]);
            }
        }
    }

    function showProductOnUpdateForm($product_id){
        $db = $this -> loadDatabase();

        $query = "SELECT * FROM products WHERE id =".$product_id;
        $products = $db->query($query);
        $products = $products-> fetch();
        return $products;
//        echo $query;
    }

    function showProductImagesOnUpdateForm($product_id){
        $db = $this -> loadDatabase();

        $query = "SELECT * FROM product_photos WHERE product_id=".$product_id;
        $images = $db->query($query);
        return $images;
//        echo $query;
    }

    function updateProduct($code, $name, $price, $description, $category_id, $supplier_id, $quantity ,$productPhotoTmpName, $productPhotoUrl ,$productPhotoName, $hot){
        $db = $this->loadDatabase();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $updated_at = date("Y-m-d h:i:s");
        $updateProduct = "UPDATE products SET code='".$code."', name='".$name."', price=".$price.", 
                        description='".$description."', category_id=".$category_id.", supplier_id=".$supplier_id.",
                        quantity=".$quantity.", updated_at='".$updated_at."', hot= ".$hot." WHERE code='".$code."'";
        $db->exec($updateProduct);

        //Insert into table product_photos
        $query = "SELECT * FROM products WHERE code ='".$code."'";
        $selectProductId = $db->query($query);
        $selectProductId = $selectProductId ->fetch();
        if($productPhotoName != ""){
            if(isset($_FILES['image'])){
                for ($i = 0; $i < count($productPhotoUrl); $i++){
                    $query = "DELETE FROM product_photos WHERE product_id=".$selectProductId['id'];
                    $db -> exec($query);
                }
                foreach ($productPhotoUrl as $url){
                    $query = "INSERT INTO product_photos(url, product_id) VALUES('".$url."', ".$selectProductId['id'].")";
                    $db -> exec($query);
                }
                for ($i = 0; $i < count($productPhotoUrl); $i++){
                    move_uploaded_file($productPhotoTmpName[$i],$productPhotoUrl[$i]);
                }
            }
        }
    }

    function deleteProduct($id){
        $db = $this->loadDatabase();

        $query = "UPDATE products SET status = 0 WHERE id=".$id;
        $db->exec($query);
    }
}