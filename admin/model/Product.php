<?php
/**
 * Created by PhpStorm.
 * User: thang
 * Date: 7/28/17
 * Time: 3:42 PM
 */
class Product{
    private $id;
    private $code;
    private $name;
    private $price;
    private $category_name;
    private $supplier_name;
    private $description;

    public function __construct($code, $name, $price, $category_name, $supplier_name, $description)
    {
        $this->code = $code;
        $this->name = $name;
        $this->price = $price;
        $this->category_name = $category_name;
        $this->supplier_name = $supplier_name;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getCategoryName()
    {
        return $this->category_name;
    }

    /**
     * @return mixed
     */
    public function getSupplierName()
    {
        return $this->supplier_name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
}
?>