<?php 
	class Supplier {
		public $id;
		public $name;
		public $address;
		public $phone;
		public $typical_photo;
		public $description;

		public function __construct($id, $name, $address, $phone,
			$typical_photo, $description) {
			$this->id = $id;
			$this->name =  $name;
			$this->address = $address;
			$this->phone = $phone;
			$this->typical_photo = $typical_photo;
			$this->description = $description;
		}
	}
?>