<?php 
	class Order {
		public $id;
		public $fullname;
		public $phone;
		public $address;
		public $email;
		public $status;

		public function __construct($id, $fullname, $phone, $address, $email, $status) {
			$this->id = $id;
			$this->fullname = $fullname;
			$this->phone = $phone;
			$this->address = $address;
			$this->email = $email;
			$this->status = $status;
		}
	}
?>