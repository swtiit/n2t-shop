<?php 
	class Category {
		public $id;
		public $name;
		public $typical_photo;
		public $description;
		public $show_menu;

		public function __construct($id, $name, $typical_photo, $description, $show_menu) {
			$this->id = $id;
			$this->name = $name;
			$this->typical_photo = $typical_photo;
			$this->description = $description;
			$this->show_menu = $show_menu;
		}
	}
?>