<?php 
	class SearchDB extends Database {
		public function getSuppliers($content) {
			$db = $this->loadDatabase();
			$query = "SELECT * FROM suppliers WHERE name LIKE '%$content%' LIMIT 10";

			$rows = $db->query($query)->fetchAll();

			$suppliers = [];
			foreach ($rows as $row) {
				$id = $row['id'];
				$name = $row['name'];
				$address = $row['address'];
				$phone = $row['phone'];
				$typical_photo = $row['typical_photo'];
				$description = $row['description'];
				$supplier = new Supplier($id, $name, $address, $phone, $typical_photo,
					$description);
				
				$suppliers[] = $supplier;
			}

			return $suppliers;
		}

		public function getUsers($content) {
			$db = $this->loadDatabase();
			$query = "SELECT * FROM users WHERE role <> 2 AND username LIKE '%$content%' LIMIT 10";

			$rows = $db->query($query)->fetchAll();

			$users = [];
			foreach ($rows as $row) {
				$id = $row['id'];
				$username = $row['username'];
				$password = $row['password'];
				$role = $row['role'];
				
				$user = new User($id, $username, $password, $role);
				
				$users[] = $user;
			}

			return $users;
		}

		public function getCategories($content){
            $db = $this->loadDatabase();
            $query = "SELECT * FROM categories WHERE name LIKE '%$content%' LIMIT 10";

            $rows = $db->query($query)->fetchAll();

            $categories = [];
            foreach ($rows as $row) {
                $id = $row['id'];
                $name = $row['name'];
                $typical_photo = $row['typical_photo'];
                $description = $row['description'];
                $show_menu = $row['show_menu'];
                $category = new Category($id, $name, $typical_photo, $description, $show_menu);
                $categories[] = $category;
            }

            return $categories;
        }

        public function getProducts($content){
            $db = $this->loadDatabase();
            $query = "SELECT * FROM products WHERE name LIKE '%$content%' LIMIT 10";

            $rows = $db->query($query)->fetchAll();

            $products = [];
            foreach ($rows as $row) {
                $id = $row['id'];
                $code = $row['code'];
                $name = $row['name'];
                $price = $row['price'];
                $category_id = $row['category_id'];
                $supplier_id = $row['supplier_id'];
                $description = $row['description'];

                $product = new Product($code, $name, $price, $category_id, $supplier_id, $description);
                $product -> setId($id);

                $products[] = $product;
            }

            return $products;
        }
	}
?>