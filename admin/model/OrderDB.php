<?php 
	require 'Order.php';
	require 'OrderDetail.php';

	class OrderDB extends Database {
		public function getOrders($start, $limit) {
			$db = $this->loadDatabase();

			$query = "SELECT * FROM orders ORDER BY status LIMIT {$start}, {$limit}";

			$rows = $db->query($query);
			$orders = [];
			foreach ($rows as $row) {
				$id = $row['id'];
				$fullname = $row['fullname'];
				$phone = $row['phone'];
				$address = $row['address'];
				$email = $row['email'];
				$status = $row['status'];
				$order = new Order($id, $fullname, $phone, $address, $email, $status);
				
				$orders[] = $order;
			}

			return $orders;
		}

		public function totalRecord() {
			$db = $this->loadDatabase();

			$query = "SELECT * FROM orders";
			$rows = $db->query($query);
			$count = $rows->fetchAll();

			return count($count);
		}

		public function create($order) {
			$db = $this->loadDatabase();

			$query = "INSERT INTO orders(fullname, phone, address, email) VALUES
				('$order->fullname', '$order->phone', '$order->address', '$order->email')";

			$db->exec($query);

			$query = "SELECT id FROM orders ORDER BY id DESC LIMIT 1";
			$result = $db->query($query);
			$result = $result->fetch();

			return $result['id'];			
		}

		public function latchOrder($order_id) {
			$db = $this->loadDatabase();

			$query = "UPDATE orders SET status = 1 WHERE id = $order_id";
			$db->exec($query);
		}

		public function detailOrder($order_id) {
			$db = $this->loadDatabase();

			$query = "SELECT * FROM order_detail WHERE order_id = $order_id";
			$rows = $db->query($query);
			$order_details = [];
			foreach ($rows as $row) {
				$id = $row['id'];
				$order_id = $row['order_id'];
				$product_id = $this->getDetailProduct($row['product_id'])['name'];
				$image = $this->getDetailProduct($row['product_id'])['image'];
				$quantities = $row['quantities'];
				$unit_price = $row['unit_price'];
				$total_price = $row['quantities'] * $row['unit_price'];

				$order_detail = new OrderDetail($id, $order_id, $product_id, $image, $quantities,
					$unit_price, $total_price);
				
				$order_details[] = $order_detail;
			}

			return $order_details;
		}

		public function getDetailProduct($product_id) {
			$db = $this->loadDatabase();

			$query = "SELECT products.name AS name, product_photos.url AS image
				FROM products JOIN product_photos ON products.id = product_photos.product_id
				WHERE products.id = $product_id";
				
			$product = $db->query($query);
			$product = $product->fetch();

			return $product;
		}
	}
?>
