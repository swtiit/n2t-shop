<?php
	require ('User.php');

	class UserDB extends Database{
		public function getUser($start, $limit) {
			$db= $this->loadDatabase();
			$query= "SELECT * FROM users WHERE role <> 2 ORDER BY id LIMIT {$start},{$limit}";
			$rows=$db->query($query);
			$users=[];
			foreach ($rows as $row){
				$id= $row['id'];
				$username = $row['username'];
				$password = md5($row['password']);
				$role = $row['role'];
				$user = new User($id, $username, $password, $role);
				$users[] = $user;
			}

			return $users;
		}

		public function totalRecord() {
			$db = $this->loadDatabase();

			$query = "SELECT * FROM users WHERE role <> 2";
			$rows = $db->query($query);
			$count = $rows->fetchAll();

			return count($count);
		}

		public function deleteUser($id){
			$db=$this->loadDatabase();
			$query= "DELETE FROM users WHERE id='$id'";
			$db->exec($query);
		}


	public function addUser($user){
		$db= $this->loadDatabase();
		$password = md5($user->password);
		$query= "INSERT INTO users (username, password, role) 
		VALUES 
		('$user->username','$password', '$user->role')";

		$db->exec($query);
	}

	public function getDataUserById($user_id) {
		$db = $this->loadDatabase();
		
		$query = "SELECT * FROM users WHERE id = $user_id";
		$result = $db->query($query);
					
		$row = $result->fetch();

		$id = $row['id'];
		$username = $row['username'];
		$password = $row['password'];
		$role = $row['role'];

		$user = new User($id, $username, $password, $role);
			
		return $user;
	}

	public function editUser($user){
		$db=$this->loadDatabase();
		$password = md5($user->password);
		
		$query= "UPDATE users SET username='$user->username',
			password='$password', role='$user->role' WHERE id='$user->id'";

		$db->exec($query);
	}

	public function resetPassword($user){
		$db=$this->loadDatabase();
		$password = md5($user->password);
		
		$query= "UPDATE users SET	password='$password' WHERE id='$user->id'";

		$db->exec($query);
	}

	public function deactiveUser($id){
		$db=$this->loadDatabase();
		$query= "UPDATE users SET role = 2 WHERE id = $id";

		$db->exec($query);
	}

	public function showUserID($id){
		$db=$this->loadDatabase();
		$query= "SELECT * FROM users WHERE id= '$id'";
		return $db->query($query);
	}

	public function checkLogin($username, $password) {
		$db = $this->loadDatabase();
		$password = md5($password);

		$query = "SELECT * FROM users WHERE username = '$username' AND
		password = '$password' AND (role = 0 OR role = 1)";
//		echo $query;
		$result = $db->query($query)->fetchAll();
		$count = count($result);

		if ($count > 0) {
			return $result;
		} else {
			return false;
		}
	}
}
?>