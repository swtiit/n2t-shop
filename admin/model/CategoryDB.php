<?php 
	require 'Category.php';
	require 'Database.php';

	class CategoryDB extends Database {
		public function getCategories($start, $limit) {
			$db = $this->loadDatabase();

			$query = "SELECT * FROM categories ORDER BY id LIMIT {$start},{$limit}";
			$rows = $db->query($query);
			$categories = [];
			foreach ($rows as $row) {
				$id = $row['id'];
				$name = $row['name'];
				$typical_photo = $row['typical_photo'];
				$description = $row['description'];
				$show_menu = $row['show_menu'];
				$category = new Category($id, $name, $typical_photo, $description, $show_menu);
				$categories[] = $category;
			}

			return $categories;
		}

		public function totalRecord() {
			$db = $this->loadDatabase();

			$query = "SELECT * FROM categories";
			$rows = $db->query($query);
			$count = $rows->fetchAll();

			return count($count);
		}

		public function insert($categoryName, $categoryDescription, $categoryRepresentPhoto ,$categoryRepresentPhotoTmpName, $show_menu){
            $db = $this->loadDatabase();

            $query = "INSERT INTO categories(name, description, typical_photo, show_menu) VALUES ('".$categoryName."','".$categoryDescription."','".$categoryRepresentPhoto."','".$show_menu."')";
            $db -> exec($query);
            if (isset($_FILES['image'])) {
                move_uploaded_file($categoryRepresentPhotoTmpName, $categoryRepresentPhoto);
            } else {
                echo "failed";
            }
        }

        public function showCategoryById($categoryId){
            $db = $this->loadDatabase();

            $query = "SELECT * FROM categories WHERE id= $categoryId";
            $category = $db -> query($query);
            $category = $category -> fetch();

            return $category;
        }

        public function update($categoryName, $categoryDescription, $categoryRepresentPhoto ,$categoryRepresentPhotoName, $id, $show_menu){
            $db = $this->loadDatabase();

            $categoryId = $id; //Get category id to update
            $query = "SELECT * FROM categories WHERE id= $categoryId";
            $category = $db -> query($query);
            $category = $category -> fetch();
            // $category['id'];

            if ($categoryRepresentPhotoName == ""){
                $query = "UPDATE categories SET name = '$categoryName',
                	description = '$categoryDescription', show_menu = '$show_menu' WHERE id = $id";
                $db -> exec($query);
            } else {
                if (isset($_FILES['image'])) {
                    $query = "UPDATE categories SET name = '$categoryName',
                    	description = '$categoryDescription', typical_photo = '$categoryRepresentPhoto',
                    	show_menu = '$show_menu' WHERE id = $id";
                    $db -> exec($query);

                    move_uploaded_file($categoryRepresentPhotoName, $categoryRepresentPhoto);
                } else {
                    echo "failed";
                }
            }
        }

        public function deleteCategory($id) {
            $db = $this->loadDatabase();

            $query = "SELECT * FROM products WHERE category_id=".$id;
            $checkProduct = $db->query($query);
            if($checkProduct){

            } else {
                $query = "DELETE FROM categories WHERE id = '$id'";
                $db->exec($query);
            }
        }
	}
?>
