<?php 
	class OrderDetail {
		public $id;
		public $order_id;
		public $product_id;
		public $image;
		public $quantities;
		public $unit_price;
		public $total_price;

		public function __construct($id, $order_id, $product_id, $image, $quantities,
			$unit_price, $total_price) {

			$this->id = $id;
			$this->order_id = $order_id;
			$this->product_id = $product_id;
			$this->image = $image;
			$this->quantities = $quantities;
			$this->unit_price = $unit_price;
			$this->total_price = $total_price;
		}
	}
?>