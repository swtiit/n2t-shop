
<?php
	require 'Supplier.php';

	class SupplierDB extends Database{
		public function getSuppliers($start, $limit) {
			$db = $this->loadDatabase();

			$query = "SELECT * FROM suppliers ORDER BY id LIMIT {$start},{$limit}";
			$rows = $db->query($query);
			$suppliers = [];
			foreach ($rows as $row) {
				$id = $row['id'];
				$name = $row['name'];
				$address = $row['address'];
				$phone = $row['phone'];
				$typical_photo = $row['typical_photo'];
				$description = $row['description'];
				$supplier = new Supplier($id, $name, $address, $phone, $typical_photo,
					$description);
				
				$suppliers[] = $supplier;
			}

			return $suppliers;
		}

		public function totalRecord() {
			$db = $this->loadDatabase();

			$query = "SELECT * FROM suppliers";
			$rows = $db->query($query);
			$count = $rows->fetchAll();

			return count($count);
		}

		public function addSupplier($supplier) {
			$db = $this->loadDatabase();

			$query = "INSERT INTO suppliers(name, address, phone, typical_photo, description)
				VALUES
				('$supplier->name', '$supplier->address', '$supplier->phone',
				'$supplier->typical_photo', '$supplier->description')";
			$db->exec($query);		
		}

		public function updateSupplier($supplier) {
			$db = $this->loadDatabase();
			
			$query = "UPDATE suppliers SET name = '$supplier->name',
				address = '$supplier->address', phone = '$supplier->phone',
				typical_photo = '$supplier->typical_photo',
				description = '$supplier->description' WHERE id = $supplier->id";
			$db->exec($query);
		}

		public function deleteSupplier($id) {
			$db = $this->loadDatabase();
			$query = "DELETE FROM suppliers WHERE id = '$id'";

			$db->exec($query);
		}

		public function getDataSupplierById($supplier_id) {
			$db = $this->loadDatabase();
			
			$query = "SELECT * FROM suppliers WHERE id = $supplier_id";
			$result = $db->query($query);
						
			$row = $result->fetch();

			$id = $row['id'];
			$name = $row['name'];
			$address = $row['address'];
			$phone = $row['phone'];
			$typical_photo = $row['typical_photo'];
			$description = $row['description'];

			$supplier = new Supplier($id, $name, $address, $phone, $typical_photo,
				$description);
				
			return $supplier;
		}
	}
?>