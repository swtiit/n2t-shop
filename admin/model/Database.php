<?php 
	require '../system/config/Config.php';

	class Database {
		public function loadDatabase() {
			$config = new Config();

			try {
				$db = new PDO($config->dsn, $config->username, $config->password);
				return $db;
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		}
	}
?>