<?php
	require_once './controller/CategoryController.php';
	require_once './controller/ProductController.php';
	require_once './controller/SupplierController.php';
  require_once './controller/UserController.php';
  require_once './controller/SearchController.php';
  require_once './controller/OrderController.php';

	$action = NULL;
	if(isset($_REQUEST['action'])) {
		$action = $_REQUEST['action'];
	}
  
  $categoryController = new CategoryController();
	$productController = new ProductController();
	$userController = new UserController();
	$supplierController = new SupplierController();
	$searchController = new SearchController();
	$orderController = new OrderController();

	switch ($action) {
		case "edit":
			$categoryController -> showFormUpdate();
		break;
		case "insert":
			$categoryController -> insertCategory();
		break;
		case "showCategoryList":
			$categoryController->getCategories();
		break;
		case "listProducts":
			$productController-> showListProduct();
		break;
		case "insertProduct":
			$productController-> insertProduct();
		break;
		case "editProduct":
			$productController-> showEditProductForm();
		break;
		case "deleteProduct";
			$productController-> deleteProduct();
		break;
		case 'list-suppliers':
			$supplierController->getSuppliers();
		break;
		case 'list-suppliers':
			$supplierController->getSuppliers();
		break;
		case 'add-supplier':
			$supplierController->newSupplier();
		break;
		case 'edit-supplier':
			$supplierController->editSupplier();
		break;
		case 'delete-supplier':
			$supplierController->deleteSupplier();
		break;
		case 'delete-category':
			$id = $_REQUEST['id'];
			$categoryController->deleteCategory($id);
		case "listUser":
			$userController->getUser();
		break;
		case "addUser":
			$userController->addUser();
		break;
		case "editUser":
			$userController->editUser();
		break;
		case 'deactiveUser':
			$userController->deactiveUser();
		break;
		case "signin":
			$userController->signin();
		break;
		case "login":
			$userController->login();
		break;
		case "logout":
			$userController->logout();
		break;
		case 'changePassword':
			$userController->resetPassword();	
		break;
		case 'search-suppliers':
			$searchController->getSuppliers();	
		break;
		case 'search-users':
			$searchController->getUsers();	
		break;
		case 'search-categories':
			$searchController->getCategories();
		break;
		case 'search-products':
			$searchController->getProducts();
		break;
		case 'getOrders':
			$orderController->getOrders();
		break;
		case 'latchOrder':
			$orderController->latchOrder();
		break;
		case 'detailOrder':
			$orderController->detailOrder();
		break;
		default:
			$categoryController->getCategories();
		break;
	}
?>
