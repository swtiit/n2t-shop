/**
 * Created by Admin on 7/29/2017.
 */

$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            $('.imgUpload').remove();
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr({'src': event.target.result, 'class': 'imgUpload'}).appendTo(placeToInsertImagePreview);

                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});

$("#formInsertProduct").submit(function (e) {
    var errors = new Array();

    if ($("#productCode").val() == ""){
        $("#productCodeError").text("Không để trống");
        errors.push(1);
    } else {
        $("#productCodeError").text("");
    }
    if ($("#productName").val() == ""){
        $("#productNameError").text("Không để trống");
        errors.push(1);
    } else {
        $("#productNameError").text("");
    }
    if($("#productPrice").val() == 0){
        $("#productPriceError").text("Không để trống");
        errors.push(1);
    } else if(!/[0-9]+/.test($("#productPrice").val()) || /\s/.test($("#productPrice").val())){
        $("#productPriceError").text("Vui lòng điền số");
        errors.push(1);
    } else {
        $("#productPriceError").text("");
    }
    if ($("#productQuantity").val() == ""){
        $("#productQuantityError").text("Không để trống");
        errors.push(1);
    }else if(!/[0-9]+/.test($("#productQuantity").val())  || /\s/.test($("#productQuantity").val())){
        $("#productQuantityError").text("Vui lòng điền số");
        errors.push(1);
    }else{
        $("#productQuantityError").text("");
    }

    if(errors.length != 0){
        e.preventDefault();
    } else{
        alert("Thêm mới thành công..");
    }
});