/**
 * Created by thang on 7/31/17.
 */
/**
 * Created by Admin on 7/29/2017.
 */

$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            $('.imgUpload').remove();
            var reader = new FileReader();
            reader.onload = function(event) {
                $($.parseHTML('<img>')).attr({'src': event.target.result, 'class': 'imgUpload'}).appendTo(placeToInsertImagePreview);
            }
            reader.readAsDataURL(input.files[0]);
        }
    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});

$("#insertCategory").submit(function (e) {
    var errors = new Array();

    if ($("#categoryName").val() == ""){
        $("#categoryNameError").text("Không để trống");
        errors.push(1);
    } else {
        $("#categoryNameError").text("");
    }

    if(errors.length != 0){
        e.preventDefault();
    } else{
        alert("Insert Successful..");
    }
});
