<?php 
	if (session_status() !== PHP_SESSION_ACTIVE) {
		session_start();
	}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
    	<?php echo isset($title) ? $title : "N2T Shop"; ?>
    </title>
    <link rel="stylesheet" href="./public/bootstraps/css/bootstrap.min.css">
    <link rel="stylesheet" href="./public/css/header.css">
    <link rel="stylesheet" href="./public/css/footer.css">
    <link rel="stylesheet" href="./public/css/category.css">
    <link rel="stylesheet" href="./public/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="./public/css/Product/ShoeDetail/shoedetail.css">
    <link rel="stylesheet" href="./public/css/Product/DeliveryInfo/deliveryinfo.css">
    <link rel="stylesheet" href="./public/css/Product/DeliveryInfo/deliveryinfo.css">
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="./public/css/home.css">
</head>
<body>
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="col-sm-2">
						<div class="logo">
							<a href="/"><img src="public/images/logo/logo.png" alt=""></a>
						</div>
				</div>
				<div class="col-sm-8">
					<div class="input-group stylish-input-group">
						<input type="text" class="form-control"  placeholder="Search" >
						<span class="input-group-addon">
							<button type="submit">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="btn-carts dropdown">
                    <a href="?action=cart" id="cart">
                      <i class="fa fa-shopping-cart"></i> Cart
                      <span class="badge badge-success">
                      	<?php echo (isset($_SESSION['carts'])) ? count($_SESSION['carts']) : 0; ?>
                      	
                      </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <nav class="navbar">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle btn-rep" data-toggle="collapse"
                            data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar_menu" id="myNavbar">
                  <ul class="nav navbar-nav" style="margin-left: 15px;">
                    <?php foreach ($menus as $menu) { ?>
                    	<li class="<?php echo ($id == $menu['id']) ?
                        "active" : ""; ?>">
                        <a class="menu-category" href="?action=show-category&id=<?php echo $menu['id']; ?>"><?php echo $menu['name']; ?></a>
                      </li>
                    <?php } ?>
                      <li class="dropdown">
                          <a class="dropdown-toggle menu-category" data-toggle="dropdown" href="#"> Danh mục khác<span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <?php foreach ($menuitems as $menuitem) { ?>
                              <li class="<?php echo ($id == $menuitem['id']) ?
                              "active" : ""; ?>">
                              <a class="menu-category" href="?action=show-category&id=<?php echo $menuitem['id']; ?>">
                                  <?php echo $menuitem['name']; ?></a>
                            </li>
                          <?php } ?>
                          </ul>
                      </li>
                  </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
