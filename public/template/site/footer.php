	<div class="footer">
		<div class="footer_category">
			<div class="container">
				<div class="row">
					<div class="footer_category">
						<div class="col-sm-8 col-sm-offset-2">
							<div class="row">
								<div class="col-sm-3">
									<div class="img_footer">
										<a href="#" title="BÚP BÊ">
										<img src="public/images/icon_footer/icon_footer_1.svg"
											alt="BÚP BÊ"></a>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="img_footer">				
										<a href="#" title="CAO GÓT">
											<img src="public/images/icon_footer/icon_footer_2.svg"
												alt="CAO GÓT"></a>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="img_footer">
										<a href="#" title="XĂNG ĐAN">
											<img src="public/images/icon_footer/icon_footer_3.svg"
												alt="XĂNG ĐAN"></a>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="img_footer">
										<a href="#" title="DÉP GUỐC">
											<img src="public/images/icon_footer/icon_footer_4.svg"
												alt="DÉP GUỐC"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer_intro">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="info_intro">
							<ul>
								<li><a href="#" title="">Tin tức, khuyến mãi N2T</a></li>
								<li><a href="#" title="">Hướng dẫn chọn cỡ giày</a></li>
								<li><a href="#" title="">Chính sách khách hàng thân thiết</a></li>
								<li><a href="#" title="">Chính sách đổi trả</a></li>
								<li><a href="#" title="">Thanh toán giao nhận</a></li>
								<li><a href="#" title="">Chính sách bảo mật</a></li>
								<li><a href="#" title="">Các thông tin khác</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer_info">
			<div class="container">
				<div class="row">
					<div class="col-sm-7">
						<div class="info_company">
							© 2015 N2T. Công ty cổ phần sản xuất thương mại dịch vụ N2T. </br>
							Văn phòng: Tầng 11, Toà nhà MD Complex, 68 Nguyễn Cơ Thạch, Mỹ Đình, Hà Nội.
						</div>
					</div>
					<div class="col-sm-5">
						<div class="social">
							<p>LIKE N2T TRÊN MẠNG XÃ HỘI</p>
							<div class="list_socials">
								<ul>
									<li><i class="fa fa-facebook-official" aria-hidden="true"></i></li>
									<li><i class="fa fa-instagram" aria-hidden="true"></i></li>
									<li><i class="fa fa-youtube" aria-hidden="true"></i></li>
									<li><i class="fa fa-pinterest" aria-hidden="true"></i></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="./public/js/jquery-2.2.4.js"></script>
<script src="./public/js/home_slie.js"></script>
<script src="./public/bootstraps/js/bootstrap.min.js"></script>
<script src="./public/bootstraps/js/shoedetail.js"></script>
</html>