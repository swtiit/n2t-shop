<?php 
	if (session_status() !== PHP_SESSION_ACTIVE) {
		session_start();
	}

	if (!isset($_SESSION['username'])) {
		header('Location: /admin?action=signin');
	}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" href="../public/bootstraps/css/bootstrap.min.css">
  <link rel="stylesheet" href="../public/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="../public/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../public/css/_all-skins.min.css">
  <link rel="stylesheet" href="../public/css/supplier/supplier.css">
  <link rel="stylesheet" href="../public/css/custom-admins.css">
  <link rel="stylesheet" href="../public/css/custom-admins.css">
  <link rel="stylesheet" href="../public/css/Product/InsertProduct.css">
	</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
	  <header class="main-header">
	    <a href="?action=showCategoryList" class="logo">
	      <span class="logo-lg"><b>Admin</b></span>
	    </a>
	    <nav class="navbar navbar-static-top">
	      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
	        <span class="sr-only">Toggle navigation</span>
	      </a>
	      <div class="navbar-custom-menu">
	        <ul class="nav navbar-nav">
	          <li class="dropdown user user-menu">
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	              <img src="../public/images/admin/user2-160x160.jpg" class="user-image" alt="User Image">
	              <span class="hidden-xs">
	              	<?php if (isset($_SESSION['username'])) { ?>
	                		<?php echo $_SESSION['username']; ?> 
	                	<?php } else { ?>
		                  Swtiit
	                  <?php } ?>
	              </span>
	            </a>
	            <ul class="dropdown-menu">
	              <li class="user-header">
	                <img src="../public/images/admin/user2-160x160.jpg" class="img-circle" alt="User Image">
	                <p>
	                	<?php if (isset($_SESSION['username'])) { ?>
	                		<?php echo $_SESSION['username']; ?> 
	                  	<small>Member since Nov. 2015</small>
	                	<?php } else { ?>
		                  Swtiit - Web Developer
		                  <small>Member since Nov. 2015</small>
	                  <?php } ?>
	                </p>
	              </li>
	              <li class="user-footer">
	                <div class="pull-left">
	                  <a href="?action=changePassword" class="btn btn-default btn-flat">Đổi mật khẩu</a>
	                </div>
	                <div class="pull-right">
	                  <a href="?action=logout" class="btn btn-default btn-flat">Đăng xuất</a>
	                </div>
	              </li>
	            </ul>
	          </li>
	        </ul>
	      </div>
	    </nav>
	  </header>

	  <aside class="main-sidebar">
	    <section class="sidebar">
	      <div class="user-panel">
	        <div class="pull-left image">
	          <img src="../public/images/admin/user2-160x160.jpg" class="img-circle" alt="User Image">
	        </div>
	        <div class="pull-left info">
	          <p><?php if (isset($_SESSION['username'])) { ?>
	                		<?php echo $_SESSION['username']; ?> 
	                	<?php } else { ?>
		                  Swtiit
	                  <?php } ?></p>
	          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
	        </div>
	      </div>
	      <ul class="sidebar-menu" data-widget="tree">
	        <li class="header">MENU</li>
	        <li class="<?php echo isset($class_admin) ? (($class_admin == "category") ? 
			        	"active" : "") : "" ?>">
	          <a href="?action=showCategoryList">
	            <i class="fa fa-th-list"></i>
	            <span>Danh mục</span>
	          </a>
	        </li>
	        <li class="<?php echo isset($class_admin) ? (($class_admin == "supplier") ? 
			        	"active" : "") : "" ?>">
	          <a href="?action=list-suppliers">
	            <i class="fa fa-building-o"></i>
	            <span>Nhà cung cấp</span>
	          </a>
	        </li>
	        <li class="<?php echo isset($class_admin) ? (($class_admin == "product") ? 
			        	"active" : "") : "" ?>">
	          <a href="?action=listProducts">
	            <i class="fa fa-product-hunt"></i>
	            <span>Sản phẩm</span>
	          </a>
	        </li>	
	        <li class="<?php echo isset($class_admin) ? (($class_admin == "order") ? 
			        	"active" : "") : "" ?>">
	          <a href="?action=getOrders">
	            <i class="fa fa-cart-plus"></i>
	            <span>Đơn hàng</span>
	          </a>
	        </li>
					 <li class="<?php echo isset($class_admin) ? (($class_admin == "users") ? 
			        	"active" : "") : "" ?>">
		          <a href="?action=listUser">
		            <i class="fa  fa-users"></i>
		            <span>Người dùng</span>
		          </a>
		        </li>
	      </ul>
	    </section>
	  </aside>

	  <div class="content-wrapper ">
	    <section class="content-header">
	      <!-- <h1 style="font-size: 40px; color: blue;">Bảng điều khiển</h1> -->
	    </section>

    <div class="content-admin-wapper">
