<?php
require_once './pages/model/Database.php';

class MainPageDB extends Database {
    public function showRecentlyAddedProducts(){
        $db = $this->loadDatabase();

        $query = "SELECT products.name, products.code, products.id, products.price, product_photos.url, products.created_at FROM products
                  JOIN product_photos ON product_photos.url = (SELECT url FROM product_photos WHERE product_id = products.id LIMIT 1) 
                   WHERE products.status = 1 ORDER BY products.created_at DESC LIMIT 5";
        $recentlyAdded = $db -> query($query);
        $recentlyAdded = $recentlyAdded ->fetchAll();
        return $recentlyAdded;
    }

    public function showHotProducts(){
        $db = $this->loadDatabase();

        $query = "SELECT * FROM products JOIN product_photos ON product_photos.url = (SELECT url FROM product_photos WHERE product_id = products.id LIMIT 1) 
        WHERE hot = 1 AND status = 1 LIMIT 5";
        $hot = $db -> query($query);
        $hot = $hot ->fetchAll();

        return $hot;
    }

    public function showShoeDetail($id){
        $db = $this ->loadDatabase();

        $query = "SELECT products.name as product_name, products.code, products.id, products.price, product_photos.url, categories.name as category_name, products.description FROM products
                  JOIN product_photos ON products.id = product_photos.product_id 
                  JOIN categories ON products.category_id = categories.id WHERE products.id = '".$id."'";
        $product = $db ->query($query);
        $product = $product -> fetchAll();

        return $product;
    }

    public function showProductwithCategory($id) {
    	$db = $this->loadDatabase();

     	$query = "SELECT * FROM products JOIN product_photos ON product_photos.id = (SELECT id FROM product_photos WHERE product_id = products.id LIMIT 1) WHERE category_id = $id";
    	$products = $db->query($query);
    	$products = $products->fetchAll();
    	
    	return $products;
    }

    public function showNewProducts() {
    	$db = $this->loadDatabase();

        $query = "SELECT * FROM products JOIN product_photos ON product_photos.id = (SELECT id FROM product_photos WHERE product_id = products.id LIMIT 1) ORDER BY id DESC";
    	$products = $db->query($query);

    	return $products;
    }

    public function showCategoryMenu() {
    	$db = $this->loadDatabase();

    	$query = "SELECT * FROM categories WHERE show_menu = 1";
    	$menus = $db->query($query);
    	$menus = $menus->fetchAll();

			return $menus;
    }

    public function showCategoryMenuItem() {
    	$db = $this->loadDatabase();

    	$query = "SELECT * FROM categories WHERE show_menu = 0";
    	$menuitems = $db->query($query);
    	$menuitems = $menuitems->fetchAll();

			return $menuitems;
    }

    public function getNameCategory($id) {
    	$db = $this->loadDatabase();

    	$query = "SELECT * FROM categories WHERE id = $id";
    	$menuitems = $db->query($query);
    	$menuitems = $menuitems->fetch();

			return $menuitems['name'];
    }
}