<?php 
	include('./public/template/site/header.php');
?>	
	<div class="slide">
		<div class="container">
			<div class="row">
				<div>
					<div class="w3-content w3-display-container max-width-slide">
					  <img class="mySlides" src="public/images/slide/slide1.jpg">
					  <img class="mySlides" src="public/images/slide/slide2.jpg">
					  <img class="mySlides" src="public/images/slide/slide3.jpg">
					  <div class="w3-center w3-container w3-section w3-large w3-text-white w3-display-bottommiddle">
					    <div class="w3-left w3-hover-text-khaki" onclick="plusDivs(-1)">&#10094;</div>
					    <div class="w3-right w3-hover-text-khaki" onclick="plusDivs(1)">&#10095;</div>
					    <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(1)"></span>
					    <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(2)"></span>
					    <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(3)"></span>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="top-5-title">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="top-5-product-title">
						<h3>TOP 05 SẢN PHẨM HOT NHẤT TUẦN</h3>
					</div>
					<div class="line-top-5-title"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="top-5-products">
		<div class="container">
	        <?php foreach ($hot as $hot): ?>
			<div class="product-width">
				<div class="row">
					<div class="img-top-5-products">
						<a href="?action=shoedetail&id=<?php echo $hot['id'] ?>">
							<img src="<?php echo $hot['url']?>">
						</a>
					</div>
					<div class="price-top-5-products">
            <div>
              <p><?php echo $hot['name'] ?></p>
            </div>
            <div class="price-product">
							<h6><?php echo number_format($hot['price'], 0, ',', ','); ?> VND</h6>
						</div>
					</div>
				</div>
			</div>
	        <?php endforeach;?>
		</div>
	</div>
	<div class="top-new-title">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="top-new-product-title">
						<h3>TOP SẢN PHẨM MỚI NHẤT TUẦN NÀY</h3>
					</div>
					<div class="line-top-new-title"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="top-5-products">
		<div class="container">
	        <?php foreach ($recentlyAdded as $recentlyAdded): ?>
			<div class="product-width">
				<div class="row">
					<div class="img-top-new-products">
	                    <a href="?action=shoedetail&id=<?php echo $recentlyAdded['id'] ?>"><img src="<?php echo $recentlyAdded['url']?>"></a>
					</div>
					<div class="price-top-new-products">
                        <div>
                            <h6><?php echo $recentlyAdded['name']?></h6>
                        </div>
						<div class="price-product">
							<h6><?php echo number_format($recentlyAdded['price'], 0, ',', ','); ?> VND</h6>
						</div>
					</div>
				</div>
			</div>
	        <?php endforeach;?>
		</div>
	</div>
	<div class="new-stars">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="new-stars-title">
						<h3>CÁC SAO VIỆT NÔ NỨC TỚI ỦNG HỘ CHƯƠNG TRÌNH CỦA N2T</h3>
					</div>
                    <div class="line-news-title"></div>
				</div>
				<div class="col-sm-3">
					<div class="img-star">
						<img src="public/images/stars/star-1.jpg">
					</div>
					<div class="title-img-star">
						<h6>Hương Giang, Bảo Thanh - Diễn viên phim "Sống chung với mẹ chồng"</h6>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="img-star">
						<img src="public/images/stars/star-2.jpg">
					</div>
					<div class="title-img-star">
						<h6>Lan Khuê - Huấn luyện viên The Face</h6>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="img-star">
						<img src="public/images/stars/star-3.jpg">
					</div>
					<div class="title-img-star">
						<h6>Angela Phương Trinh - Diễn viên</h6>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="img-star">
						<img src="public/images/stars/star-4.jpg">
					</div>
					<div class="title-img-star">
						<h6>Diễm My 9X - Diễn viên</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php 
	include('./public/template/site/footer.php');
?>