<?php 
	if (session_status() !== PHP_SESSION_ACTIVE) {
		session_start();
	}

	if (isset($_GET['submit'])) {
		$products = array('id' => $_GET['id'], 'name' => $_GET['name'],
			'code' => $_GET['code'], 'price' => $_GET['price'], 'image' => $_GET['image'],
			'size' => $_GET['size'], 'quantity' => $_GET['quantity']);

	$_SESSION["carts"][$products['id']] = $products;
	}
?>

<?php 
	include('./public/template/site/header.php');
?>

<div class="order">
	<div class="container">
    <div class="row">
      <div class="cart">
        <h4>GIỎ HÀNG CỦA BẠN</h4>
          <form method="GET" action="">
          	<?php $totalPrice = 0; ?>
          	<?php if (isset($_SESSION["carts"])) {
          		foreach ($_SESSION["carts"] as $key => $value) { ?>
	          		<div class="just">
			            <div class="media">
			              <a class="pull-left" href="#">
			                <img class="media-object" src="<?php echo $value['image']; ?>"
			                width="100px" height="100px">
			              </a>
			              <div class="media-body">
			                <a href=""><strong class="giay"><?php echo $value['name']; ?></strong></a><br>
			                <span class="money"><?php echo number_format($value['price'], 0, ',', ','); ?> VND</span><br>
			                <p>Size : <?php echo $value['size']; ?></p>
			                <input type="hidden" name="id[]" value="<?php echo $value['id']; ?>">
			                <p>Số lượng : <input type="number" name="quantity[]"
			                	class="quantity" value="<?php echo $value['quantity']; ?>"></p>
			                <a type="button" href="?action=deleteCart&id=<?php echo $value['id']; ?>" 
			                	class="btn btn-danger btn-delete-cart">Xóa</a>
			              </div>
			            </div>
			          </div>
		          <?php $totalPrice += $value['price'] * $value['quantity']; ?>
          	<?php } } ?>
      							
						<input type="hidden" name="action" value="updateCart">
	          <p class="tongtien">Tổng tiền trong giỏ hàng: <span class="money"><?php echo $totalPrice; ?> VND</span></p>
	          <a class="thanhtoan" href="/?action=paymentCarts">THANH TOÁN</a>
	          <input type="submit" name="submit" class="maner" value="CẬP NHẬT GIỎ HÀNG">
	          <a class="continue_buy" href="/" title="">Tiếp tục mua hàng</a>
          </form>
      </div>
    </div>
	</div>
</div>

<?php 
	include('./public/template/site/footer.php');
?>