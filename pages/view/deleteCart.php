<?php
	if (session_status() !== PHP_SESSION_ACTIVE) {
		session_start();
	}

	$id = $_GET['id'];
	unset($_SESSION["carts"][$id]);

	header("Location: /?action=cart");
?>