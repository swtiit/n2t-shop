<?php $title = "Thông tin sản phẩm" ?>
<?php include "public/template/site/header.php" ?>

<div id="content" class="container">
	<div class="row">
		<div class="col-md-5 slides">
			<?php foreach ($products as $product): ?>
				<img class="mySlides w3-animate-opacity" src="<?php echo $product['url']?>">
			<?php endforeach;?>
			<button class="w3-white w3-border btnLeft" onclick="plusDivs(-1)">&#10094;</button>
			<button class="w3-white w3-border btnRight" onclick="plusDivs(+1)">&#10095;</button>
			<hr>
			<?php foreach ($products as $productPhoto): ?>
				<img src="<?php echo $productPhoto['url'] ?>" class="imgList">
			<?php endforeach;?>
		</div>
		<div class="col-md-4 detail">
			<form method="GET" action="">
				<input type="hidden" name="action" value="cart">
				<input type="hidden" name="id" value="<?php echo $product['id'] ?>">
				<input type="hidden" name="name" value="<?php echo $product['product_name'] ?>">
				<input type="hidden" name="code" value="<?php echo $product['code'] ?>">
				<input type="hidden" name="price" value="<?php echo $product['price'] ?>">
				<input type="hidden" name="image" value="<?php echo $product['url'] ?>">
				<h4 class="shoename"><?php echo $product['product_name'] ?></h4>
				<h5 class="shoeid">Mã SP: <?php echo $product['code'] ?></h5>
				<p class="price"><?php echo number_format($product['price'], 0, ',', ',') ?> VND</p>
				<h5>Chọn size:</h5>
				<input class="w3-radio" type="radio" name="size" value="35" checked>35
				<input class="w3-radio" type="radio" name="size" value="36">36
				<input class="w3-radio" type="radio" name="size" value="37">37
				<input class="w3-radio" type="radio" name="size" value="38">38
				<h5>Số lượng:</h5>
				<input type="number" name="quantity" class="form-control quantity" value="1">
				<input type="submit" name="submit" class="btn btn-danger btn-lg" value="MUA NGAY">
				<br>
			</form>
		</div>
		<div class="col-md-3 sidebar">
			<div>
				<div class="menu-sidebar">
					<h4 class="note">Sẽ có tại nhà bạn</h4>
					<span>từ 1-5 ngày làm việc</span>
				</div>
				<div class="menu-sidebar">
					<h4><img src="public/images/iconpagedetail/icon-deliver.png"> Giao hàng miễn phí</h4>
					<span>sản phẩm trên 300.000đ</span>
				</div>
				<div class="menu-sidebar">
					<h4><img src="public/images/iconpagedetail/icon-day90.png"> Đổi trả miễn phí</h4>
					<span>đổi trả miễn phí 90 ngày</span>
				</div>
				<div class="menu-sidebar">
					<h4><img src="public/images/iconpagedetail/icon-pay.png"> Thanh toán</h4>
					<span>thanh toán khi nhận hàng</span>
				</div>
			</div>
			<br>
			<a href="https://www.lazada.vn/?offer_name=VN+-+PB+Deeplink+Generator&affiliate_id=110188&offer_id=
			511&transaction_id=1021880ad8096aea7e49850f71c205&affiliate_name=VN_Accesstradevn&aff_source=4567650847
			994398604"><img src="https://sanhangonline.com/wp-content/uploads/2016/10/4cdd0678fd0d085c9879b4d6af3eca75.gif" alt="commercial" title="Lazada" style="width: 100%"></a>
		</div>
	</div>
	<br>
	<h3 class="detail-label">Thông số kỹ thuật</h3>
	<div class="row" id="description">
		<div class="col-md-4">
			<img src="<?php echo $product['url'] ?>">
		</div>
		<div class="col-md-8">
			<table class="w3-table-all w3-card-4">
				<tr>
				<td>Mã sản phẩm:</td>
					<td><?php echo $product['code'] ?></td>
				</tr>
				<tr>
					<td>Kiểu dáng:</td>
					<td><?php echo $product['category_name'] ?></td>
				</tr>
				<tr>
					<td>Giá:</td>
					<td><?php echo number_format($product['price'], 0, ',', ',') ?><sup>VND</sup></td>
				</tr>
				<tr>
					<td>Size:</td>
					<td>35-36-37-38</td>
				</tr>
				<tr>
					<td>Mô tả:</td>
					<td>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<br>

<?php include "public/template/site/footer.php" ?>
