<?php
	if (session_status() !== PHP_SESSION_ACTIVE) {
		session_start();
	}

	$quantity = $_GET['quantity'];
	$id = $_GET['id'];

	$i = 0;
	foreach ($_SESSION["carts"] as $cart) {
		$_SESSION["carts"][$id[$i]]['quantity'] = $quantity[$i];
		$i ++;
	}

	header("Location: /?action=cart");
?>