<?php 
	include('./public/template/site/header.php');
?>

<div class="top-5-title">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="top-5-product-title">
					<h3><?php echo strtoupper($nameCategory); ?></h3>
				</div>
				<div class="line-top-5-title"></div>
			</div>
		</div>
	</div>
	</div>
	<div class="top-5-products">
		<div class="container">
	    <?php foreach ($products as $product): ?>
				<div class="product-width">
					<div class="row">
						<div class="img-top-5-products">
		          <a href="?action=shoedetail&id=<?php echo $product['id'] ?>">
		          	<img src="<?php echo $product['url']?>"></a>
						</div>
						<div class="price-top-5-products">
							 <div>
	              <p><?php echo $product['name'] ?></p>
	            </div>
	            <div class="price-product">
								<h6><?php echo number_format($product['price'], 0, ',', ',') ?> VND</h6>
							</div>
						</div>
					</div>
				</div>
	    <?php endforeach;?>
		</div>
	</div>

<?php 
	include('./public/template/site/footer.php');
?>