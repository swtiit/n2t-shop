<?php 
	$title = "Thanh toán";

	if (session_status() !== PHP_SESSION_ACTIVE) {
		session_start();
	}

 	include "public/template/site/header.php"; 
?>

<div class="container">
	<form action="?action=paymentCartsDone" method="POST">
	<div class="row">
		<div class="col-sm-6">
	    <h3>Thông tin giao hàng</h3>
	    <div class="form-group">
				<label for="name">Họ và tên:
					<strong class="notnull"> ( * ) </strong></label>
				<input type="text" class="form-control margin-input" name="fullname">
			</div>
			<div class="form-group">
				<label for="name">Email:
					<strong class="notnull"> ( * ) </strong></label>
				<input type="email" class="form-control margin-input" name="email">
			</div>
			<div class="form-group">
				<label for="name">Số điện thoại:
					<strong class="notnull"> ( * ) </strong></label>
				<input type="text" class="form-control margin-input" name="phone">
			</div>
			<div class="form-group">
				<label for="name"> Địa chỉ:
					<strong class="notnull"> ( * ) </strong></label>
				<input type="text" class="form-control margin-input" name="address">
			</div>
			<div class="form-group">
				<label for="name"> Ghi chú: </label>
				<textarea name="" class="form-control margin-input"></textarea>
			</div>
		</div>
    <div class="col-sm-6">
      <div id="order">
      	<?php $totalPrice = 0; ?>
      	<?php if (isset($_SESSION['carts'])) {
      		foreach ($_SESSION['carts'] as $cart) { ?>
      			<div class="product row">
		          <div class="img-detail col-md-4">
		            <img class="w3-round-large" src="<?php echo $cart['image']; ?>">
		            <span class="badge"><?php echo $cart['quantity'] ?></span>
		          </div>
		          <div class="text-detail col-md-6">
		            <h5><?php echo $cart['name']; ?></h5>
		            <h6><?php echo $cart['code']; ?></h6>
		            <h6><?php echo $cart['size']; ?></h6>
		          </div>
		          <div class="price-detail col-md-2">
		            <h4><?php echo number_format(($cart['quantity'] * $cart['price']), 0, ',', ','); ?> VND</h4>
		          </div>
		        </div>
						<input type="hidden" name="product_id[]" value="<?php echo $cart['id']; ?>">
						<input type="hidden" name="quantities[]" value="<?php echo $cart['quantity']; ?>">
						<input type="hidden" name="unit_price[]" value="<?php echo $cart['price']; ?>">
		      	<?php $totalPrice += ($cart['price'] * $cart['quantity']); ?>
      	<?php } } ?>
        <br>
      </div>
      <hr>
			<h5>Tổng cộng: <span class="money"><?php echo number_format($totalPrice, 0, ',', ','); ?> VND</span></h5>
			<input type="submit" name="submit" class="btn btn-primary btn-lg" value="Hoàn tất đơn hàng">
    </div>
  </div>
	</div>
</form>
</div>

<?php include "public/template/site/footer.php" ?>