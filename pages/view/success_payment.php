<?php 
	$title = "Thành công";

	if (session_status() !== PHP_SESSION_ACTIVE) {
		session_start();
	}

 	include "public/template/site/header.php"; 
?>
	
<div class="container">
	<div class="col-sm-4 col-sm-offset-4">
		</br></br>
		<a href="/"><img src="./public/images/icon/success-icon.png" class="image-success"></a></br></br>
		<div class="alert alert-success text-align" role="alert">
			<strong>Bạn đã gửi đơn hàng thành công!</strong>
		</div>
		<div class="alert alert-info text-align" role="alert">
			<strong>Bạn sẽ nhận được hàng sau 3 ngày</strong>
		</div>
	</div>
</div>
		
<?php include "public/template/site/footer.php" ?>