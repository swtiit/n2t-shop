<?php
require_once './pages/model/MainPageDB.php';
require_once './admin/model/OrderDB.php';
require_once './admin/model/OrderDetailDB.php';

class MainPageController{
    public function showMainPage(){
        $mainPageDB = new MainPageDB();
        $recentlyAdded = $mainPageDB->showRecentlyAddedProducts();
        $hot = $mainPageDB -> showHotProducts();
        $menus = $mainPageDB->showCategoryMenu();
        $menuitems = $mainPageDB->showCategoryMenuItem();

        require('pages/view/MainPage.php');
    }

    public function showShoeDetail(){
        $mainPageDB = new MainPageDB();
        $id = $_REQUEST['id'];
        $products = $mainPageDB -> showShoeDetail($id);
        $menus = $mainPageDB->showCategoryMenu();
        $menuitems = $mainPageDB->showCategoryMenuItem();

        require_once ('pages/view/ShoeDetail.php');
    }

    public function showCategory() {
    	$mainPageDB = new MainPageDB();
    	$id = $_REQUEST['id'];
    	$products = $mainPageDB->showProductwithCategory($id);
    	$menus = $mainPageDB->showCategoryMenu();
    	$menuitems = $mainPageDB->showCategoryMenuItem();
    	$nameCategory = $mainPageDB->getNameCategory($id);


    	require ('./pages/view/show_with_category.php');
    }


    public function showHangMoiVe() {
    	$mainPageDB = new MainPageDB();
    	$products = $mainPageDB->showNewProducts();
    	$menus = $mainPageDB->showCategoryMenu();
    	$menuitems = $mainPageDB->showCategoryMenuItem();

    	require ('./pages/view/show_with_category.php');
    }

    public function showCart() {
    	$mainPageDB = new MainPageDB();
    	$menus = $mainPageDB->showCategoryMenu();
    	$menuitems = $mainPageDB->showCategoryMenuItem();	

    	require ('./pages/view/cart.php');
    }

    public function updateCart() {
    	$mainPageDB = new MainPageDB();
    	$menus = $mainPageDB->showCategoryMenu();
    	$menuitems = $mainPageDB->showCategoryMenuItem();	

    	require ('./pages/view/updateCart.php');
    }

    public function deleteCart() {
    	$mainPageDB = new MainPageDB();
    	$menus = $mainPageDB->showCategoryMenu();
    	$menuitems = $mainPageDB->showCategoryMenuItem();	

    	require ('./pages/view/deleteCart.php');
    }

    public function paymentCarts() {
    	$mainPageDB = new MainPageDB();
    	$menus = $mainPageDB->showCategoryMenu();
    	$menuitems = $mainPageDB->showCategoryMenuItem();	

    	require ('./pages/view/payment.php');
    }

    public function paymentCartsDone() {
    	$mainPageDB = new MainPageDB();
    	$orderDB = new OrderDB();
    	$orderDetailDB = new OrderDetailDB();

    	$menus = $mainPageDB->showCategoryMenu();
    	$menuitems = $mainPageDB->showCategoryMenuItem();

    	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    		$fullname = $_POST['fullname'];
    		$phone = $_POST['phone'];
    		$address = $_POST['address'];
    		$email = $_POST['email'];

    		$order = new Order(0, $fullname, $phone, $address, $email, 0);
    		$order_id = $orderDB->create($order);

    		for ($i=0; $i < count($_POST['product_id']); $i++) { 
    			$product_id = $_POST['product_id'][$i];
	    		$quantities = $_POST['quantities'][$i];
	    		$unit_price = $_POST['unit_price'][$i];

	    		$order_detail = new OrderDetail(0, $order_id, $product_id, 0, $quantities, $unit_price, 0);
	    		$orderDetailDB->create($order_detail);
    		}
    		
    		if (session_status() !== PHP_SESSION_ACTIVE) {
					session_start();
				}

				unset($_SESSION['carts']);
    	}

    	require ('./pages/view/success_payment.php');
    }
}