<?php
	require_once './pages/controller/MainPageController.php';
	$action = NULL;
	if(isset($_REQUEST['action'])){
		$action = $_REQUEST['action'];
	}

	$mainPageController = new MainPageController();
	
	switch ($action){
	case "shoedetail":
		$mainPageController->showShoeDetail();
	break;
	case "show-category":
		$mainPageController->showCategory();
	break;
	case "cart":
		$mainPageController->showCart();
	break;
	case "updateCart":
		$mainPageController->updateCart();
	break;
	case "deleteCart":
		$mainPageController->deleteCart();
	break;
	case "paymentCarts":
		$mainPageController->paymentCarts();
	break;
	case "paymentCartsDone":
		$mainPageController->paymentCartsDone();
	break;
	default:
		$mainPageController->showMainPage();
	break;            
	}
?>