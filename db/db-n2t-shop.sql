create database n2tshop;

use n2tshop;

create table categories(
	id int(11) not null auto_increment primary key unique,
    name nvarchar(255) not null,
    typical_photo nvarchar(1000),
    description nvarchar(1000)
);

create table suppliers(
	id int(11) not null auto_increment primary key unique,
    name nvarchar(255) not null,
    address nvarchar(255),
    phone varchar(15),
    typical_photo nvarchar(1000),
    description nvarchar(1000)
);

create table products (
	id int(11) not null auto_increment primary key unique,
    code varchar(15) not null unique,
    name nvarchar(255) not null,
    price int(11) not null,
    category_id int(11) not null,
    supplier_id int(11) not null,
    description nvarchar(1000),
	constraint fk_category foreign key(category_id) references categories(id),
    constraint fk_supplier foreign key(supplier_id) references suppliers(id)
);

create table users(
	id int(11) not null auto_increment primary key unique,
    username nvarchar(255) not null unique,
    password nvarchar(255) not null   
);

create table orders(
	id int(11) not null primary key auto_increment unique,
    fullname nvarchar(255) not null,
    phone varchar(15) not null,
    address nvarchar(255) not null,
    email nvarchar(100) not null,
    status int(1) not null default 0
);

create table order_detail(
	id int(11) not null primary key auto_increment unique,
    order_id int(11) not null,
    product_id int(11) not null,
    quantities int(11) not null,
    unit_price int(11) not null,
    constraint fk_order foreign key(order_id) references orders(id),
    constraint fk_product foreign key(product_id) references products(id)
);















